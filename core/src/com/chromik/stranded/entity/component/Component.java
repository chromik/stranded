package com.chromik.stranded.entity.component;

import com.chromik.stranded.core.io.FileReader;
import com.chromik.stranded.core.io.FileWriter;
import com.chromik.stranded.entity.Entity;

public class Component {
	protected Entity entity; // entity that has this component


	public void init() {
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}


	public Entity getEntity() {
		return this.entity;
	}

	public void write(FileWriter writer) {

	}

	public void load(FileReader reader) {

	}
}