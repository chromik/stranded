package com.chromik.stranded.entity.component;

import com.chromik.stranded.entity.entities.item.inventory.InventoryComponent;
import com.chromik.stranded.entity.entities.item.tile.multitile.chest.ChestComponent;
import com.chromik.stranded.entity.entities.world.ClockComponent;
import com.chromik.stranded.util.log.Log;

import java.util.HashMap;

public class SaveableComponents {

	public static String[] list = {
		"PositionComponent",
		"SizeComponent",
		"InventoryComponent",
		"ChestComponent",
		"ClockComponent"
	};

	public static HashMap<String, Class<? extends Component>> map = new HashMap<>();

	public static void init() {
		map.put("PositionComponent", PositionComponent.class);
		map.put("SizeComponent", SizeComponent.class);
		map.put("InventoryComponent", InventoryComponent.class);
		map.put("ChestComponent", ChestComponent.class);
		map.put("ClockComponent", ClockComponent.class);

		Log.debug("Testing map: " + map.get("PositionComponent"));
	}
}