package com.chromik.stranded.entity.component.physics;

import com.chromik.stranded.entity.component.Component;

public class CollisionComponent extends Component {
	public boolean solid = true; // shows if the entity is solid
	public boolean onGround = true; // shows if entity is on the ground
	public float weight = 0.2f; // how fast the entity falls
}