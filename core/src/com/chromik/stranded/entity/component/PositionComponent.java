package com.chromik.stranded.entity.component;

import com.chromik.stranded.core.io.FileReader;
import com.chromik.stranded.core.io.FileWriter;

import java.io.IOException;


public class PositionComponent extends Component {
	public float x;
	public float y;

	@Override
	public void write(FileWriter writer) {
		try {
			writer.writeFloat(this.x);
			writer.writeFloat(this.y);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void load(FileReader reader) {
		try {
			this.x = reader.readFloat();
			this.y = reader.readFloat();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
}