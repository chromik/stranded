package com.chromik.stranded.entity.component;

import com.badlogic.gdx.graphics.g2d.TextureRegion;


public class TextureComponent extends Component {

	public TextureRegion texture;
}