package com.chromik.stranded.entity.component;

import com.chromik.stranded.core.io.FileReader;
import com.chromik.stranded.core.io.FileWriter;

import java.io.IOException;

public class SizeComponent extends Component {
	public float width;
	public float height;

	@Override
	public void write(FileWriter writer) {
		try {
			writer.writeFloat(this.width);
			writer.writeFloat(this.height);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void load(FileReader reader) {
		try {
			this.width = reader.readFloat();
			this.height = reader.readFloat();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
}