package com.chromik.stranded.entity;

import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.util.log.Log;

import java.lang.reflect.Constructor;
import java.util.HashMap;

public class Entity implements Comparable<Entity> {

	protected final HashMap<Class<? extends Component>, Component> components = new HashMap<>();
	protected byte zIndex = 0;

	@SuppressWarnings("all")
	public Entity(Class<? extends Component>... components) {
		for (Class<? extends Component> type : components) {
			this.addComponent(type);
		}
	}

	public void render() {
	}

	public void renderUi() {
	}

	public final Component addComponent(Class<? extends Component>... component) {
		Component first = null;

		for (Class<? extends Component> c : component) {
			Component instance = null;

			try {
				Constructor<?> constructor = c.getConstructor();
				instance = (Component) constructor.newInstance();
				instance.setEntity(this);
				instance.init();

				if (first == null) {
					first = instance;
				}
			} catch (Exception exception) {
				exception.printStackTrace();
				Log.error("Failed to create a component " + c.getName());
			}

			this.components.put(c, instance);
		}

		return first;
	}

	public void removeComponent(Class<? extends Component> component) {
		this.components.remove(component);
	}

	public void removeAll() {
		this.components.clear();
	}

	public boolean hasComponent(Class<? extends Component> component) {
		return this.components.containsKey(component);
	}

	public <T extends Component> T getComponent(Class<T> component) {
		return (T) this.components.get(component);
	}

	public void setZIndex(byte zIndex) {
		this.zIndex = zIndex;
	}

	public HashMap<Class<? extends Component>, Component> getComponents() {
		return this.components;
	}

	public byte getZIndex() {
		return this.zIndex;
	}

	@Override
	public int compareTo(Entity other) {
		byte z1 = this.getZIndex();
		byte z2 = other.getZIndex();
		if (z1 > z2) {
			return 1;
		} else if (z1 < z2) {
			return -1;
		}
		return 0;
	}
}