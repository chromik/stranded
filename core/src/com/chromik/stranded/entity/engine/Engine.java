package com.chromik.stranded.entity.engine;

import com.chromik.stranded.core.Debug;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.Players;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.*;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.engine.system.systems.*;
import com.chromik.stranded.entity.entities.camera.Camera;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.creature.Creature;
import com.chromik.stranded.entity.entities.item.inventory.InventoryComponent;
import com.chromik.stranded.entity.entities.world.Sky;
import com.chromik.stranded.entity.entities.world.WorldIO;
import com.chromik.stranded.util.log.Log;

import java.util.*;

public class Engine {
	private static final ArrayList<System> systems = new ArrayList<>();
	private static final ArrayList<Entity> entities = new ArrayList<>();
	private static final Map<Class<?>, HashSet<Entity>> componentToEntity = new HashMap<>();


	public static void init() {
		initSystems();

		Entity camera = new Camera();
		TargetComponent target = camera.getComponent(TargetComponent.class);
		CameraComponent cam = camera.getComponent(CameraComponent.class);
		IdComponent id = camera.getComponent(IdComponent.class);

		id.id = "main";

		Entity uiCamera = new Camera();
		CameraComponent uiCam = uiCamera.getComponent(CameraComponent.class);
		IdComponent uiId = uiCamera.getComponent(IdComponent.class);

		uiCam.camera.position.x = uiCam.camera.viewportWidth / 2;
		uiCam.camera.position.y = uiCam.camera.viewportHeight / 2;
		uiId.id = "ui";

		addEntity(camera);
		addEntity(uiCamera);
		addEntity(WorldIO.load("test", "flat")); // TODO: CREATE MENU FOR HANDLING INIT OF WORLD

		Players.players = Engine.getWithAllTypes(InventoryComponent.class);
		Creature player;

		if (Players.players.size() == 0) {
			player = Assets.creatures.create("st:player");

			PositionComponent position = player.getComponent(PositionComponent.class);

			position.x = 1256;
			position.y = 6000;

			Engine.addEntity(player);
			Players.players.add(player);
		} else {
			Log.info("Loaded player");
			player = (Creature) Players.players.stream().findFirst().get();
		}
		Players.clientPlayer = player;

		target.target = player;

		PositionComponent position = player.getComponent(PositionComponent.class);
		SizeComponent size = player.getComponent(SizeComponent.class);

		InventoryComponent inventory = player.getComponent(InventoryComponent.class);
		inventory.inventory[0].item = Assets.items.get("st:stone_pickaxe");
		inventory.inventory[0].count = 1;

		cam.camera.position.x = position.x + size.width / 2;
		cam.camera.position.y = position.y + size.height / 2;

		addEntity(new Sky());
	}

	private static void initSystems() {
		addSystem(CameraSystem.getInstance());
		addSystem(SaveSystem.getInstance());
		addSystem(AnimationSystem.getInstance());
		addSystem(MovementSystem.getInstance());
		addSystem(CollisionSystem.getInstance());
		addSystem(InventorySystem.getInstance());
		addSystem(ClockSystem.getInstance());
		addSystem(InteractionSystem.getInstance());
		addSystem(UiSystem.getInstance());
		addSystem(AiSystem.getInstance());
		addSystem(ChestSystem.getInstance());
		addSystem(BiomeSystem.getInstance());

		if (Debug.lightEnabled) {
			addSystem(new LightSystem());
		}
	}

	public static void sendMessage(SystemMessage message) {
		for (System system : systems) {
			system.handleMessage(message);
		}
	}

	public static void sendMessage(SystemMessage message, Object object) {
		for (System system : systems) {
			system.handleMessage(message);
		}
	}

	public static void update(float delta) {
		for (System system : systems) {
			system.update(delta);
		}
	}

	public static void addSystem(System system) {
		systems.add(system);
	}

	@SuppressWarnings("unchecked")
	public static HashSet<Entity> getWithAllTypes(Class<? extends Component>... types) {
		HashSet<Entity> intersection = null;
		for (Class<? extends Component> type : types) {
			HashSet<Entity> compEntities = getWithAnyTypes(type);
			if (intersection == null) {
				/* set return set to the current temp set. Will be used as abase-line for future
				operations */
				intersection = compEntities;
			} else {
				// intersection of return set and temp set
				intersection.retainAll(compEntities);
			}
		}
		return intersection;
	}

	public static HashSet<Entity> getWithAnyTypes(Class<? extends Component>... types) {
		HashSet<Entity> set = new HashSet<>();
		for (Class<? extends Component> type : types) {
			HashSet<Entity> compSet = componentToEntity.get(type);
			if (compSet != null) {
				set.addAll(componentToEntity.get(type));
			}
		}
		return set;
	}

	public static void addEntity(Entity entity) {
		// find position to insert entity at. Searches where to insert by comparing entity z-indices
		int pos = Collections.binarySearch(entities, entity);
		// adjust for correct insertion point if necessary
		if (pos < 0) {
			pos = -pos - 1;
		}
		// insert at position
		entities.add(pos, entity);
		// update caches
		for (Class<? extends Component> componentClass : entity.getComponents().keySet()) {
			HashSet<Entity> set = componentToEntity.getOrDefault(componentClass, new HashSet<>());
			set.add(entity);
			componentToEntity.putIfAbsent(componentClass, set);
		}
		sendMessage(SystemMessage.Type.ENTITIES_UPDATED.create());
	}

	public static void removeEntity(Entity entity) {
		entities.remove(entity);

		// remove from caches
		for (Class<?> componentClass : entity.getComponents().keySet()) {
			HashSet<Entity> set = componentToEntity.getOrDefault(componentClass, new HashSet<>());
			set.remove(entity);
			componentToEntity.putIfAbsent(componentClass, set);
		}
		sendMessage(SystemMessage.Type.ENTITIES_UPDATED.create());
	}

	public static ArrayList<Entity> getEntities() {
		return entities;
	}
}