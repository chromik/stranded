package com.chromik.stranded.entity.engine;

import java.util.HashMap;
import java.util.Map;

public class SystemMessage {

	private final Map<String, Object> data = new HashMap<>();
	private final Type type;

	public SystemMessage(Type context) {
		this.type = context;
	}

	public SystemMessage set(String key, Object value) {
		data.put(key, value);
		return this;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		return (T) data.get(key);
	}

	public <T> T getOr(String key, T fallback) {
		T value = get(key);
		if (value == null) return fallback;
		return value;
	}

	public Type getType() {
		return type;
	}

	public enum Type {
		ENTITIES_UPDATED, // entity added/removed
		TILE_UPDATE, // tile updated (destroyed/placed)
		WALL_UPDATE, // wall updated (destroyed/placed)
		WINDOW_RESIZED, // window resized
		SAVE, // game needs to save
		LOAD_CHUNK; // chunk loaded

		public SystemMessage create() {
			return new SystemMessage(this);
		}
	}
}