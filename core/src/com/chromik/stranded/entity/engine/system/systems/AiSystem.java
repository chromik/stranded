package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.creature.AiComponent;

import java.util.HashSet;
import java.util.Objects;

public class AiSystem implements System {

	private static AiSystem instance;

	public static AiSystem getInstance() {
		if (instance == null) {
			instance = new AiSystem();
		}
		return instance;
	}

	private AiSystem() {
	}

	private HashSet<Entity> entities = new HashSet<>();


	@Override
	public void update(float delta) {
		for (Entity entity : this.entities) {
			AiComponent ai = entity.getComponent(AiComponent.class);
			ai.ai.update(ai.getEntity());
		}
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAllTypes(AiComponent.class);
		}
	}
}