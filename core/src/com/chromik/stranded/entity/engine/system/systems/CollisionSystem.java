package com.chromik.stranded.entity.engine.system.systems;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.component.physics.CollisionComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.creature.AiComponent;
import com.chromik.stranded.entity.entities.item.tile.Tile;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.util.collision.Collider;
import com.chromik.stranded.util.struct.Pair;
import com.chromik.stranded.util.struct.Pairs;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CollisionSystem implements System {

	private static CollisionSystem instance;

	private CollisionSystem() {
	}

	public static CollisionSystem getInstance() {
		if (instance == null) {
			instance = new CollisionSystem();
		}
		return instance;
	}

	private HashSet<Entity> entities = new HashSet<>();

	@Override
	public void update(float delta) {
		SizeComponent worldSize = World.getInstance().getComponent(SizeComponent.class);
		float width = worldSize.width * Chunk.SIZE * Tile.SIZE;
		float height = worldSize.height * Chunk.SIZE * Tile.SIZE;
		Set<Pair<Entity>> pairs = Pairs.setOf(this.entities);
		for (Pair<Entity> pair : pairs) {
			// skip comparison to self
			if (pair.isSame()) {
				continue;
			}
			// retrieve first entity information
			Entity first = pair.left;
			PositionComponent pos1 = first.getComponent(PositionComponent.class);
			SizeComponent size1 = first.getComponent(SizeComponent.class);
			// skip comparison if missing the required components
			if (pos1 == null && size1 == null) {
				continue;
			}
			pos1.x = Math.max(Math.min(pos1.x, width - Tile.SIZE - size1.width), Tile.SIZE);
			pos1.y = Math.max(Math.min(pos1.y, height - Tile.SIZE - size1.height), Tile.SIZE);
			// retrieve second entity information
			Entity second = pair.right;
			PositionComponent pos2 = second.getComponent(PositionComponent.class);
			SizeComponent size2 = second.getComponent(SizeComponent.class);
			// skip comparison if missing the required components
			if (pos2 == null || size2 == null) {
				continue;
			}

			Rectangle object1 = new Rectangle(pos1.x, pos1.y, size1.width, size1.height);
			Rectangle object2 = new Rectangle(pos2.x, pos2.y, size2.width, size2.height);

			if (Collider.hitTest(object1, object2)) {
				AiComponent ai1 = first.getComponent(AiComponent.class);
				AiComponent ai2 = second.getComponent(AiComponent.class);

				if (ai1 != null) {
					ai1.ai.collide(first, second);
				}

				if (ai2 != null) {
					ai2.ai.collide(second, first);
				}

				if (ai1 != null || ai2 != null) {
					return; // don't check not updated list
				}
			}
		}
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAllTypes(CollisionComponent.class);
		}
	}
}