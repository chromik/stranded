package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.WorldIO;

import java.util.Objects;


public class SaveSystem implements System {

	private static SaveSystem instance;

	private SaveSystem() {
	}

	public static SaveSystem getInstance() {
		if (instance == null) {
			instance = new SaveSystem();
		}
		return instance;
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.SAVE)) {
			WorldIO.write(World.getInstance());
		}
	}
}