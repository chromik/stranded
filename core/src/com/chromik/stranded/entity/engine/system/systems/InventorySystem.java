package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.item.ItemUseComponent;
import com.chromik.stranded.entity.entities.item.inventory.InventoryComponent;
import com.chromik.stranded.entity.entities.item.inventory.ItemComponent;

import java.util.HashSet;
import java.util.Objects;

public class InventorySystem implements System {
	private static InventorySystem instance;

	private InventorySystem() {
	}

	public static InventorySystem getInstance() {
		if (instance == null) {
			instance = new InventorySystem();
		}
		return instance;
	}

	private HashSet<Entity> entities = new HashSet<>();

	@Override
	public void update(float delta) {
		for (Entity entity : this.entities) {
			InventoryComponent inventory = entity.getComponent(InventoryComponent.class);

			for (ItemComponent item : inventory.inventory) {
				if (!item.isEmpty()) {
					ItemUseComponent use = item.item.getComponent(ItemUseComponent.class);
					use.currentTime = Math.max(0, use.currentTime - delta);
				}
			}
		}
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAllTypes(InventoryComponent.class);
		}
	}
}