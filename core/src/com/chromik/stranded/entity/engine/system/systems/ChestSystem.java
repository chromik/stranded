package com.chromik.stranded.entity.engine.system.systems;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.item.tile.multitile.chest.ChestComponent;
import com.chromik.stranded.entity.entities.item.tile.multitile.chest.ChestRegistry;
import com.chromik.stranded.util.collision.Collider;

import java.util.HashSet;
import java.util.Objects;

public class ChestSystem implements System {

	private HashSet<Entity> entities = new HashSet<>();

	public static ChestSystem instance;

	private ChestSystem() {
	}


	public static ChestSystem getInstance() {
		if (instance == null) {
			instance = new ChestSystem();
		}
		return instance;
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAnyTypes(ChestComponent.class);
		}
	}

	public ChestRegistry find(short x, short y) {
		for (Entity entity : this.entities) {
			PositionComponent position = entity.getComponent(PositionComponent.class);

			Rectangle object = new Rectangle(position.x, position.y, 4, 4);
			Rectangle cursor = new Rectangle(x, y, 1, 1);

			if (Collider.hitTest(object, cursor)) {
				return (ChestRegistry)entity;
			}
		}

		return null;
	}
}