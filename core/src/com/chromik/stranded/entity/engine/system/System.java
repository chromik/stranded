package com.chromik.stranded.entity.engine.system;

import com.chromik.stranded.entity.engine.SystemMessage;


public interface System {

	default void update(float delta) {
	}

	default void handleMessage(SystemMessage message) {
	}
}