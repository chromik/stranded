package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.world.ClockComponent;

import java.util.HashSet;
import java.util.Objects;

public class ClockSystem implements System {

	private static ClockSystem instance;

	private ClockSystem() {
	}

	public static ClockSystem getInstance() {
		if (instance == null) {
			instance = new ClockSystem();
		}
		return instance;
	}

	private HashSet<Entity> entities  = new HashSet<>();


	@Override
	public void update(float delta) {
		for (Entity entity : this.entities) {
			ClockComponent clock = entity.getComponent(ClockComponent.class);
			float change = clock.speed * delta;

			if (delta > 0) {
				clock.second += change;

				while (clock.second >= 60) {
					clock.second -= 60;
					clock.minute += 1;

					if (clock.minute >= 60) {
						clock.minute = 0;
						clock.hour += 1;

						if (clock.hour >= 24) {
							clock.hour = 0;
							clock.day += 1;
						}
					}
				}
			}
		}
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAllTypes(ClockComponent.class);
		}
	}
}