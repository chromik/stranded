package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.ui.UiElement;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

public class UiSystem implements System {

	private static UiSystem instance; // main instance
	private HashMap<String, Entity> elements; // all in-game cameras

	private UiSystem() {
		this.elements = new HashMap<>();
	}

	public static UiSystem getInstance() {
		if (instance == null) {
			instance = new UiSystem();
		}
		return instance;
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			HashSet<Entity> entities = Engine.getWithAllTypes(IdComponent.class);
			this.elements.clear();

			for (Entity entity : entities) {
				this.elements.put(entity.getComponent(IdComponent.class).id, entity);
			}
		}
	}

	public UiElement get(String id) {
		return (UiElement) this.elements.get(id);
	}
}