package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.creature.AnimationComponent;

import java.util.HashSet;
import java.util.Objects;

public class AnimationSystem implements System {

	private static AnimationSystem instance;

	private AnimationSystem() {
	}

	public static AnimationSystem getInstance() {
		if (instance == null) {
			instance = new AnimationSystem();
		}
		return instance;
	}

	private HashSet<Entity> entities = new HashSet<>();

	@Override
	public void update(float delta) {
		for (Entity entity : this.entities) {
			AnimationComponent animation = entity.getComponent(AnimationComponent.class);

			if (animation.current != null) {
				animation.current.update(delta);
			}
		}
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAllTypes(AnimationComponent.class);
		}
	}
}