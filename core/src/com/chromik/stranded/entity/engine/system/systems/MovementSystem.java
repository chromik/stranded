package com.chromik.stranded.entity.engine.system.systems;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.core.Debug;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.component.physics.AccelerationComponent;
import com.chromik.stranded.entity.component.physics.CollisionComponent;
import com.chromik.stranded.entity.component.physics.VelocityComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.item.tile.Tile;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.util.collision.Collider;

import java.util.HashSet;
import java.util.Objects;


public class MovementSystem implements System {

	private static MovementSystem instance;

	private MovementSystem() {
	}

	public static MovementSystem getInstance() {
		if (instance == null) {
			instance = new MovementSystem();
		}
		return instance;
	}

	private HashSet<Entity> entities = new HashSet<>();

	@Override
	public void update(float delta) {
		for (Entity entity : this.entities) {
			PositionComponent position = entity.getComponent(PositionComponent.class);
			VelocityComponent velocity = entity.getComponent(VelocityComponent.class);
			AccelerationComponent acceleration = entity.getComponent(AccelerationComponent.class);
			CollisionComponent collision = entity.getComponent(CollisionComponent.class);

			acceleration.y -= collision.weight;

			velocity.x += acceleration.x;
			velocity.y += acceleration.y;

			position.x += velocity.x;

			if (collision != null && collision.solid) {
				if (this.collidesWithWorld(entity)) {
					position.x -= velocity.x;
					velocity.x = 0;
				}
			}

			position.y += velocity.y;

			if (collision != null) {
				collision.onGround = false;

				if (collision.solid) {
					if (this.collidesWithWorld(entity)) {
						position.y -= velocity.y;
						velocity.y = 0;

						collision.onGround = true;
					}
				}
			}

			acceleration.x = 0;
			acceleration.y = 0;
		}
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAllTypes(VelocityComponent.class, AccelerationComponent.class);
		}
	}

	private boolean collidesWithWorld(Entity entity) {

		PositionComponent position = entity.getComponent(PositionComponent.class);
		SizeComponent size = entity.getComponent(SizeComponent.class);

		short xStart = (short) (position.x / Tile.SIZE);
		short yStart = (short) (position.y / Tile.SIZE);

		for (short y = yStart; y < yStart + size.height / Tile.SIZE + 1; y++) {
			for (short x = xStart; x < xStart + size.width / Tile.SIZE + 1; x++) {
				String id = World.getInstance().getBlock(x, y);

				if (id != null) {
					Block block = (Block) Assets.items.get(id);

					Rectangle object = new Rectangle(position.x + 0.5f, position.y + 0.5f, size.width, size.height);
					Rectangle world = new Rectangle(x * Tile.SIZE, y * Tile.SIZE, Tile.SIZE, Tile.SIZE);

					if (block.getComponent(CollisionComponent.class).solid && Collider.hitTest(object, world)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}