package com.chromik.stranded.entity.engine.system.systems;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.biome.Biome;
import com.chromik.stranded.entity.entities.world.biome.BiomeIndexComponent;
import com.chromik.stranded.util.camera.CameraHelper;
import com.chromik.stranded.util.log.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BiomeSystem implements System {
	private Biome current;
	private static BiomeSystem instance;

	public static BiomeSystem getInstance() {
		if (instance == null) {
			instance = new BiomeSystem();
		}
		return instance;
	}

	private BiomeSystem() {
		instance = this;
		this.current = Assets.biomes.get("st:forest");

		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduler.scheduleAtFixedRate(() -> checkBiome(), 1, 1, TimeUnit.SECONDS);
	}

	public void checkBiome() {
		HashMap<String, MutableInt> blocks = new HashMap<>();
		Rectangle screen = CameraHelper.getCameraRectangleInBlocks();

		for (int x = (int) screen.x; x < screen.x + screen.width; x++) {
			for (int y = (int) screen.y; y < screen.y + screen.height; y++) {
				String id = World.getInstance().getBlock(x, y);

				if (id != null) {
					MutableInt value = blocks.get(id);

					if (value == null) {
						blocks.put(id, new MutableInt());
					} else {
						value.increment();
					}
				}
			}
		}

		for (Map.Entry<String, Biome> pair : Assets.biomes.getAll().entrySet()) {
			BiomeIndexComponent index = pair.getValue().getComponent(BiomeIndexComponent.class);
			boolean match = true;

			for (Map.Entry<String[], Integer> types : index.needed.entrySet()) {
				boolean found = false;

				for (String type : types.getKey()) {
					if (blocks.get(type).get() >= types.getValue()) {
						found = true;
						break;
					}
				}

				if (!found) {
					match = false;
					break;
				}
			}

			if (match) {
				Log.debug("Found biome: " + pair.getValue().getComponent(IdComponent.class).id);
				this.current = pair.getValue();

				break;
			}
		}
	}

	public Biome getCurrentBiome() {
		return this.current;
	}

	private class MutableInt {

		private int value = 1;

		public void increment() {
			this.value++;
		}
		public int get() {
			return this.value;
		}
	}
}