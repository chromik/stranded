package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.util.log.Log;

import com.google.common.base.Objects;

public class LightSystem implements System {

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equal(message.getOr("action", null), "set")) {
			try {
				int x = (int)message.get("x") / Chunk.SIZE;
				int y = (int)message.get("y") / Chunk.SIZE;
				
				World.getInstance().getChunk(x, y).calculateLighting(1f);
				for (int chunkX = -1; chunkX <= 1; chunkX++) {
					for (int chunkY = -1; chunkY <= 1; chunkY++) {
						if (chunkX != 0 || chunkY != 0) {
							Chunk chunk = World.getInstance().getChunk(x + chunkX, y + chunkY);
							if (chunk != null)chunk.calculateLighting(1f);
						}
					}
				}
			} catch (Exception exception) {
				exception.printStackTrace();
				Log.error("Failed to recalculate light");
			}
		}
	}
}