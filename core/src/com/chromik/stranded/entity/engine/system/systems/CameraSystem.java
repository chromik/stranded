package com.chromik.stranded.entity.engine.system.systems;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.component.TargetComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.camera.Camera;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.tile.Tile;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.graphics.PerfectViewport;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;

public class CameraSystem implements System {

	private static CameraSystem instance;
	private HashMap<String, Entity> cameras = new HashMap<>();
	private PerfectViewport viewport = new PerfectViewport();

	private CameraSystem() {
	}

	public static CameraSystem getInstance() {
		if (instance == null) {
			instance = new CameraSystem();
		}
		return instance;
	}

	@Override
	public void update(float delta) {
		for (Map.Entry<String, Entity> pair : this.cameras.entrySet()) {
			Camera camera = (Camera) pair.getValue();
			TargetComponent component = camera.getComponent(TargetComponent.class);
			CameraComponent cam = camera.getComponent(CameraComponent.class);

			if (component.target != null) {
				Entity target = component.target;
				PositionComponent position = target.getComponent(PositionComponent.class);
				SizeComponent size = target.getComponent(SizeComponent.class);

				float x = position.x + size.width / 2;
				float y = position.y + size.height / 2;
				float dx = x - cam.camera.position.x;
				float dy = y - cam.camera.position.y;

				if (Math.abs(dx) + Math.abs(dy) > 20) {
					cam.camera.position.x += dx * 4f * delta;
					cam.camera.position.y += dy * 4f * delta;
				}

				SizeComponent worldSize = World.getInstance().getComponent(SizeComponent.class);

				float width = worldSize.width * Chunk.SIZE * Tile.SIZE;
				float height = worldSize.height * Chunk.SIZE * Tile.SIZE;
				float halfDisplayWidth = cam.camera.viewportWidth / 2;
				float halfDisplayHeight = cam.camera.viewportHeight / 2;

				// Auto floor it:
				cam.camera.position.x = (int) Math.max(Math.min(cam.camera.position.x, width - halfDisplayWidth - Tile.SIZE), halfDisplayWidth + Tile.SIZE);
				cam.camera.position.y = (int) Math.max(Math.min(cam.camera.position.y, height - halfDisplayHeight - Tile.SIZE), halfDisplayHeight + Tile.SIZE);
			}

			cam.camera.update();
		}
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			HashSet<Entity> entities = Engine.getWithAllTypes(TargetComponent.class, CameraComponent.class, IdComponent.class);
			this.cameras.clear();

			for (Entity entity : entities) {
				this.cameras.put(entity.getComponent(IdComponent.class).id, entity);
			}

			if (this.cameras.size() > 0) {
				CameraComponent camera = this.get("main").getComponent(CameraComponent.class);
				this.viewport.setCamera(camera.camera);
			}
		}
	}

	public Camera get(String id) {
		return (Camera) this.cameras.get(id);
	}
}