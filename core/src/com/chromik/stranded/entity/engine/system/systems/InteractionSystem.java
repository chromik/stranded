package com.chromik.stranded.entity.engine.system.systems;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.engine.system.System;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.item.tile.Tile;
import com.chromik.stranded.entity.entities.item.tile.interactable.Interactable;
import com.chromik.stranded.entity.entities.item.tile.interactable.InteractionComponent;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.util.collision.Collider;
import com.chromik.stranded.util.input.Input;
import com.chromik.stranded.util.input.SimpleInputProcessor;

import java.util.HashSet;
import java.util.Objects;

public class InteractionSystem implements System, SimpleInputProcessor {

	private HashSet<Entity> entities = new HashSet<>();
	private static InteractionSystem instance;

	private InteractionSystem() {
		Input.multiplexer.addProcessor(this);
	}

	public static InteractionSystem getInstance() {
		if (instance == null) {
			instance = new InteractionSystem();
		}
		return instance;
	}

	@Override
	public void handleMessage(SystemMessage message) {
		if (Objects.equals(message.getType(), SystemMessage.Type.ENTITIES_UPDATED)) {
			this.entities = Engine.getWithAllTypes(InteractionComponent.class);
		}
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		CameraComponent camera = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);
		Vector3 mouse = camera.camera.unproject(new Vector3(screenX, screenY, 0));

		for (Entity entity : this.entities) {
			if (entity instanceof Interactable) {
				Interactable interactable = (Interactable) entity;

				PositionComponent position = entity.getComponent(PositionComponent.class);
				SizeComponent size = entity.getComponent(SizeComponent.class);
				boolean collides;

				if (position != null && size != null) {
					Rectangle object = new Rectangle(position.x, position.y, size.width, size.width);
					Rectangle cursor = new Rectangle(mouse.x, mouse.y, 1, 1);
					collides = Collider.hitTest(object, cursor);

				} else {
					collides = interactable.checkOverlap((int) mouse.x, (int) mouse.y);
				}

				// test collision with the mouse
				if (collides) {
					interactable.onClick((int) mouse.x, (int) mouse.y);
					return true;
				}
			}
		}

		short x = (short) Math.floor(mouse.x / Tile.SIZE);
		short y = (short) Math.floor(mouse.y / Tile.SIZE);

		String id = World.getInstance().getBlock(x, y);

		if (id != null) {
			Block block = (Block) Assets.items.get(id);

			if (block instanceof Interactable) {
				Interactable interactable = (Interactable) block;
				interactable.onClick((int) mouse.x, (int) mouse.y);
			}
		}

		return false;
	}
}