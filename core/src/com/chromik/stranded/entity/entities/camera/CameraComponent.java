package com.chromik.stranded.entity.entities.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.graphics.GameScreenOptions;

public class CameraComponent extends Component {
	public final OrthographicCamera camera = new OrthographicCamera(GameScreenOptions.WIDTH, GameScreenOptions.HEIGHT);
}