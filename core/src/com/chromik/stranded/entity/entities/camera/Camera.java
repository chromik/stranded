package com.chromik.stranded.entity.entities.camera;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.TargetComponent;

public class Camera extends Entity {
	public Camera() {
		super(TargetComponent.class, CameraComponent.class, IdComponent.class);
	}
}