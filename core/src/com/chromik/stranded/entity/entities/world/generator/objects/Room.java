package com.chromik.stranded.entity.entities.world.generator.objects;

import com.chromik.stranded.util.math.NumberUtil;
import com.chromik.stranded.util.math.RandomGenerator;

public class Room {
    public int xPos;                      // The x coordinate of the lower left tile of the room.
    public int yPos;                      // The y coordinate of the lower left tile of the room.
    public int roomWidth;                     // How many tiles wide the room is.
    public int roomHeight;                    // How many tiles high the room is.
    public Direction enteringCorridor;    // The direction of the corridor that is entering this room.


    // This is used for the first room.  It does not have a Corridor parameter since there are no corridors yet.
    public void SetupRoom (IntRange widthRange, IntRange heightRange, int columns, int rows)
    {
        // Set a random width and height.
        roomWidth = widthRange.getRandom();
        roomHeight = heightRange.getRandom();

        // Set the x and y coordinates so the room is roughly in the middle of the board.
        xPos = Math.round(columns / 2f - roomWidth / 2f);
        yPos = Math.round(rows / 2f - roomHeight / 2f);
    }


    // This is an overload of the SetupRoom function and has a corridor parameter that represents the corridor entering the room.
    public void SetupRoom (IntRange widthRange, IntRange heightRange, int columns, int rows, Corridor corridor)
    {
        // Set the entering corridor direction.
        enteringCorridor = corridor.direction;

        // Set random values for width and height.
        roomWidth = widthRange.getRandom();
        roomHeight = heightRange.getRandom();

        switch (corridor.direction)
        {
            // If the corridor entering this room is going north...
            case North:
                // ... the height of the room mustn't go beyond the board so it must be clamped based
                // on the height of the board (rows) and the end of corridor that leads to the room.
                roomHeight = NumberUtil.clamp(roomHeight, 1, rows - corridor.getEndPositionY());

                // The y coordinate of the room must be at the end of the corridor (since the corridor leads to the bottom of the room).
                yPos = corridor.getEndPositionY();

                // The x coordinate can be random but the left-most possibility is no further than the width
                // and the right-most possibility is that the end of the corridor is at the position of the room.
                xPos = RandomGenerator.random(corridor.getEndPositionX() - roomWidth + 1, corridor.getEndPositionX());

                // This must be clamped to ensure that the room doesn't go off the board.
                xPos = NumberUtil.clamp(xPos, 0, columns - roomWidth);
                break;
            case East:
                roomWidth = NumberUtil.clamp(roomWidth, 1, columns - corridor.getEndPositionX());
                xPos = corridor.getEndPositionY();

                yPos = RandomGenerator.random(corridor.getEndPositionY() - roomHeight + 1, corridor.getEndPositionY());
                yPos = NumberUtil.clamp(yPos, 0, rows - roomHeight);
                break;
            case South:
                roomHeight = NumberUtil.clamp(roomHeight, 1, corridor.getEndPositionY());
                yPos = corridor.getEndPositionY() - roomHeight + 1;

                xPos = RandomGenerator.random(corridor.getEndPositionX() - roomWidth + 1, corridor.getEndPositionX());
                xPos = NumberUtil.clamp(xPos, 0, columns - roomWidth);
                break;
            case West:
                roomWidth = NumberUtil.clamp(roomWidth, 1, corridor.getEndPositionX());
                xPos = corridor.getEndPositionY() - roomWidth + 1;

                yPos = RandomGenerator.random(corridor.getEndPositionY() - roomHeight + 1, corridor.getEndPositionY());
                yPos = NumberUtil.clamp(yPos, 0, rows - roomHeight);
                break;
        }
    }
}