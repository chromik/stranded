package com.chromik.stranded.entity.entities.world.generator.objects;


public enum Direction {
    North(0), East(1), South(2), West(3);

    private final int value;

    public static Direction getDirection(int value) {
        if (value == 0) {
            return North;
        } else if (value == 1) {
            return South;
        } else if (value == 2) {
            return East;
        } else if (value == 3) {
            return  West;
        }
        return null;
    }

    private Direction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
