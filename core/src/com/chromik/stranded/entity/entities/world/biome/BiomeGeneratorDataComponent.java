package com.chromik.stranded.entity.entities.world.biome;

import com.chromik.stranded.entity.component.Component;


public class BiomeGeneratorDataComponent extends Component {
	public String soil;
	public String soil_wall;
	public String grass;
	public String grass_wall;
}