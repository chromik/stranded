package com.chromik.stranded.entity.entities.world.biome;

import com.chromik.stranded.entity.component.Component;

import java.util.HashMap;

public class BiomeSpawnComponent extends Component {
	public int maxSpawns;
	public HashMap<String, Float> spawnRate = new HashMap<>();
}