package com.chromik.stranded.entity.entities.world.generator.tasks;

import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;

public class CaveGeneratorSimplexTask extends AbstractGeneratorTask {

    private static CaveGeneratorSimplexTask instance = null;

    private CaveGeneratorSimplexTask() {
        super();
    }

    public static CaveGeneratorSimplexTask getInstance() {
        if (instance == null) {
            instance = new CaveGeneratorSimplexTask();
        }
        return instance;
    }

    @Override
        public void run() {
        // TODO: Better offset math than this
        int seedXOffset = 13; // TODO SEED -> generator.world.getSeed();
        int seedYOffset = 3897; // TODO SEED -> generator.world.getSeed();
        // TODO: These numbers shouldn't be hard-coded
        // TODO: Let the user choose these variables when generating a world.
        int caveBiasDepth = 30;
        int octaves = 4;
        float roughness = 0.6f;
        /**
         * Scale of the cave. Larger values result in more open-space caverns. Lower
         * values result in tight corridors.
         */
        float caveScale = 34f;
        float scale = 1f / caveScale;
        float cut = 0f;
        boolean[][] solidMap = new boolean[worldWidth][worldHeight];

        for (int x = 0; x < worldWidth; x++) {
            int antiCaveBiasHeight = getHighest(x) - caveBiasDepth;
            for (int y = 0; y < worldHeight; y++) {

                // Get noise value at the coordinates
                float f = SimplexNoise.octavedNoise(x + seedXOffset, y + seedYOffset,
                        octaves, roughness, scale);

                if (y >= antiCaveBiasHeight) {
                    // Average the value with a negative number to lessen the
                    // effect of cave generation above (Y = antiCaveBiasHeight)
                    f -= Math.abs(Math.sin(((float) x) / 30f) / 1.5f);
                    f /= 4;
                }
                // Solidity is determined if the noise (which is [-1,1] -
                // antiCaveBias) is less than a given value. If cut is 0.5f, 75%
                // of the land should end up being solid, 25% will become caves.
                boolean isSolid = f <= cut;
                solidMap[x][y] = isSolid;
            }
        }

        for (int chunkX = 0; chunkX < worldWidth / Chunk.SIZE; chunkX++) {
            for (int chunkY = 0; chunkY < worldHeight / Chunk.SIZE; chunkY++) {
                Chunk chunk = world.getChunk(chunkX, chunkY);
                for (int x = 0; x < Chunk.SIZE; x++) {
                    for (int y = 0; y < Chunk.SIZE; y++) {

                        // Skip existing air blocks
                        if (chunk.getBlock(x, y) == null) {
                            continue;
                        }
                        // Insert caves
                        if (!solidMap[x][y]) {
                            chunk.setBlock(null, x, y);
                            chunk.setWall(null, x, y);
                        } else {
                            // For non caves, apply grass borders.
                            // Cut off any blocks with too few neighbors to prevent
                            // the some of the floating blocks.
                            int neighbors = calculateNeighbors(worldWidth, worldHeight, solidMap,
                                    chunkX * Chunk.SIZE + x, chunkY * Chunk.SIZE + y);

                            if (neighbors <= 3) {
                                chunk.setBlock(null, x, y);
                                chunk.setWall(null, x, y);

                            } else if (neighbors != 8) {
                                chunk.setBlock("st:grass", x, y);
                                chunk.setWall("st:grass_wall", x, y);
                            }
                            chunk.setData(TileHelper.main.create(), x, y);
                        }
                    }
                }
            }
        }

        for (int chunkX = 0; chunkX < worldWidth / Chunk.SIZE; chunkX++) {
            for (int chunkY = 0; chunkY < worldHeight / Chunk.SIZE; chunkY++) {
                Chunk chunk = world.getChunk(chunkX, chunkY);
                for (int x = 0; x < Chunk.SIZE; x++) {
                    for (int y = 0; y < Chunk.SIZE; y++) {
                        // Skip existing air blocks
                        if (chunk.getBlock(x, y) == null) {
                            continue;
                        }
                        // Insert caves
                        if (!solidMap[x][y]) {
                            chunk.setBlock(null, x, y);
                            chunk.setWall(null, x, y);
                        } else {
                            // For non caves, apply grass borders.
                            // Cut off any blocks with too few neighbors to prevent
                            // the some of the floating blocks.
                            int neighbors = calculateNeighbors(
                                worldWidth,
                                worldHeight,
                                solidMap,
                                chunkX * Chunk.SIZE +  x,
                                chunkY * Chunk.SIZE + y
                            );

                            if (neighbors <= 3) {
                                chunk.setBlock(null, x, y);
                                chunk.setWall(null, x, y);
                            } else if (neighbors != 8) {
                                chunk.setBlock("st:grass", x, y);
                                chunk.setWall("st:grass_wall", x, y);
                            }
                            chunk.setData(TileHelper.main.create(), x, y);
                        }
                    }
                }
            }
        }
    }
}