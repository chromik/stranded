package com.chromik.stranded.entity.entities.world.biome;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.IdComponent;

public class Biome extends Entity {
	public Biome() {
		super(IdComponent.class, BiomeGeneratorDataComponent.class, BiomeSpawnComponent.class, BiomeIndexComponent.class);
	}
}