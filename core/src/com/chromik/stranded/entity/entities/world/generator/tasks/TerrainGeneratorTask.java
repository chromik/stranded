package com.chromik.stranded.entity.entities.world.generator.tasks;

import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;

import java.util.Random;

public class TerrainGeneratorTask extends AbstractGeneratorTask {

	private static TerrainGeneratorTask instance;

	private TerrainGeneratorTask() {
		super();
	}

	public static TerrainGeneratorTask getInstance() {
		if (instance == null) {
			instance = new TerrainGeneratorTask();
		}
		return instance;
	}

	@Override
	public void run() {
		double[] points = new double[worldWidth];

		int max = worldHeight - 250;
		int min = worldHeight - 550;

		for (int i = 0; i < worldWidth; i++) {
			points[i] = new Random().nextInt((max - min) + 1) + min;
		}

		for (int j = 0; j < 100; j++) {
			for (int i = 1; i < worldWidth - 1; i++) {
				points[i] = (points[i - 1] + points[i + 1]) / 2;
			}
		}


		for (int chunkX = 0; chunkX < worldWidth / Chunk.SIZE; chunkX++) {
			for (int chunkY = 0; chunkY < worldHeight / Chunk.SIZE; chunkY++) {
				Chunk chunk = world.getChunk(chunkX, chunkY);
				for (int x = 0; x < Chunk.SIZE; x++) {
					int yMax = (int) points[chunkX * Chunk.SIZE + x];

					for (int y = 0; y < Chunk.SIZE; y++) {

						if (chunkY * Chunk.SIZE + y  == yMax) {
							chunk.setBlock("st:grass", x, y);
							chunk.setWall("st:grass_wall", x, y);
						} else if (chunkY * Chunk.SIZE + y < yMax) {
							chunk.setBlock("st:dirt", x, y);
							chunk.setWall("st:dirt_wall", x, y);
						}

						chunk.setData(TileHelper.main.create(), x, y);
					}
				}
			}
		}

	}
}
