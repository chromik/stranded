package com.chromik.stranded.entity.entities.world.chunk;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.util.log.Log;

public class Chunk extends Entity {
	
	private static int iterations = 7;
	private static float lightFade = 1f / iterations;
	private static float sqrt2 = 1.41421356237f;

	public static int SIZE = 64; // DEFAULT=32

	private String[] blocks; // block id's
	private String[] walls; // wall id's
	private float[] light;
	private int[] data; // 8 bit blocks, 8 bit walls, 16 bits general

	public Chunk(int x, int y) {
		super(PositionComponent.class);

		PositionComponent position = this.getComponent(PositionComponent.class);

		position.x = x;
		position.y = y;

		int size = SIZE * SIZE;

		this.blocks = new String[size];
		this.walls = new String[size];
		this.light = new float[size];
		this.data = new int[size];
	}


	public String getBlock(int x, int y) {
		if (this.isOut(x, y)) {
			return null;
		}
		return this.blocks[this.getIndex(x, y)];
	}


	public void setBlock(String value, int x, int y) {
		if (this.isOut(x, y)) {
			return;
		}
		this.blocks[this.getIndex(x, y)] = value;
	}

	public String getWall(int x, int y) {
		if (this.isOut(x, y)) {
			return null;
		}

		return this.walls[this.getIndex(x, y)];
	}

	public void setWall(String value, int x, int y) {
		if (this.isOut(x, y)) {
			return;
		}

		this.walls[this.getIndex(x, y)] = value;
	}

	public float getLight(int x, int y) {
		if (this.isOut(x, y)) {
			return 0.0f;
		}

		return this.light[this.getIndex(x, y)];
	}

	public void setLight(float value, int x, int y) {
		if (this.isOut(x, y)) {
			return;
		}

		this.light[this.getIndex(x, y)] = value;
	}

	public int getData(int x, int y) {
		if (this.isOut(x, y)) {
			return 0;
		}

		return this.data[this.getIndex(x, y)];
	}

	public void setData(int value, int x, int y) {
		if (this.isOut(x, y)) {
			return;
		}

		this.data[this.getIndex(x, y)] = value;
	}

	public int toRelativeX(int x) {
		return x - this.getX() * SIZE;
	}

	public int toRelativeY(int y) {
		return y - this.getY() * SIZE;
	}

	public int getX() {
		return (int) this.getComponent(PositionComponent.class).x;
	}

	public int getY() {
		return (int) this.getComponent(PositionComponent.class).y;
	}

	public void calculateLighting(float globalLight) {
		PositionComponent pos = this.getComponent(PositionComponent.class);
		Log.debug("Calculating lighting for chunk (" + (int)pos.x + ", " + (int)pos.y + ")");
		
		final int chunkX = (int)(pos.x * SIZE);
		final int chunkY = (int)(pos.y * SIZE);
		
		//Calculate global light.
		for (int x = 0; x < SIZE; x++) {
			int top = World.getInstance().getHighest((short)(chunkX + x)) - chunkY;
			
			for (int y = SIZE - 1; y >= 0; y--) {
				if (y >= top) {
					setLight(globalLight, x, y);
				} else {
					setLight(0f, x, y);
				}
			}
		}
		
		//Grab all emitters in the area.
		for (int x = 0; x < SIZE; x++) {
			for (int y = 0; y < SIZE; y++) {
				String id = World.getInstance().getBlock((short)(x + chunkX), (short)(y + chunkY));
				if (id != null) {
					Block block = Assets.items.getUnsafe(id);
					if (block.isEmitter()) {
						//For when blocks get brightness
						//setLight(block.getBrightness(), x, y);
						setLight(1.0f, x, y);
					}
				}
			}
		}

		float light;
		float lightFade;
		for (int i = 0; i < iterations; i++) {
			for (int x = chunkX; x < chunkX + SIZE; x++) {
				for (int y = chunkY; y < chunkY + SIZE; y++) {
					light = getLight(x - chunkX, y - chunkY);
					lightFade = Chunk.lightFade;
					light = Math.max(light, lightCandidate(x - 1, y, lightFade));//Left
					light = Math.max(light, lightCandidate(x + 1, y, lightFade));//Right
					light = Math.max(light, lightCandidate(x, y - 1, lightFade));//Bottom
					light = Math.max(light, lightCandidate(x, y + 1, lightFade));//Top
					
					lightFade *= sqrt2;
					light = Math.max(light, lightCandidate(x - 1, y + 1, lightFade));//Top-Left
					light = Math.max(light, lightCandidate(x + 1, y + 1, lightFade));//Top-Right
					light = Math.max(light, lightCandidate(x - 1, y - 1, lightFade));//Bottom-Left
					light = Math.max(light, lightCandidate(x + 1, y - 1, lightFade));//Bottom-Right
					setLight(light, x - chunkX, y - chunkY);
				}
			}
		}
	}
	
	private float lightCandidate(int x, int y, float lightFade) {
		if (World.getInstance().getBlock(x, y) != null)lightFade *= 1.5;
		return World.getInstance().getLight(x, y) - lightFade;
	}

	private int getIndex(int x, int y) {
		return x + y * SIZE;
	}

	private boolean isOut(int x, int y) {
		return x < 0 || y < 0 || x > SIZE - 1 || y > SIZE -1;
	}
}