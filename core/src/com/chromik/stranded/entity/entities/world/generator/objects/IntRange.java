package com.chromik.stranded.entity.entities.world.generator.objects;


import com.chromik.stranded.util.math.RandomGenerator;

import java.io.Serializable;

public class IntRange implements Serializable {
    public int m_Min;       // The minimum value in this range.
    public int m_Max;       // The maximum value in this range.


    // Constructor to set the values.
    public IntRange(int min, int max) {
        m_Min = min;
        m_Max = max;
    }


    // Get a random value from the range.
    public int getRandom()
    {
        return RandomGenerator.random(m_Min, m_Max);
    }
}