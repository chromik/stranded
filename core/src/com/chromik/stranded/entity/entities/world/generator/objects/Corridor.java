package com.chromik.stranded.entity.entities.world.generator.objects;


import com.chromik.stranded.util.math.NumberUtil;
import com.chromik.stranded.util.math.RandomGenerator;

import static com.chromik.stranded.entity.entities.world.generator.objects.Direction.*;

public class Corridor
{
    public int startXPos;         // The x coordinate for the start of the corridor.
    public int startYPos;         // The y coordinate for the start of the corridor.
    public int corridorLength;            // How many units long the corridor is.
    public Direction direction;   // Which direction the corridor is heading from it's room.


    // Get the end position of the corridor based on it's start position and which direction it's heading.
    public int getEndPositionX()
    {
            if (direction == North || direction == South) {
                return startXPos;
            }
            if (direction == East) {
                return startXPos + corridorLength - 1;
            }
            return startXPos - corridorLength + 1;
    }


    public int getEndPositionY()
    {
        if (direction == East || direction == West) {
            return startYPos;
        }
        if (direction == North) {
            return startYPos + corridorLength - 1;
        }
        return startYPos - corridorLength + 1;
    }


    public void SetupCorridor (Room room, IntRange length, IntRange roomWidth, IntRange roomHeight, int columns, int rows, boolean firstCorridor)
    {
        // Set a random direction (a random index from 0 to 3, cast to Direction).
        direction = RandomGenerator.randomDirection();

        // Find the direction opposite to the one entering the room this corridor is leaving from.
        // Cast the previous corridor's direction to an int between 0 and 3 and add 2 (a number between 2 and 5).
        // Find the remainder when dividing by 4 (if 2 then 2, if 3 then 3, if 4 then 0, if 5 then 1).
        // Cast this number back to a direction.
        // Overall effect is if the direction was South then that is 2, becomes 4, remainder is 0, which is north.
        Direction oppositeDirection = getDirection((room.enteringCorridor.getValue() + 2) % 4);

        // If this is noth the first corridor and the randomly selected direction is opposite to the previous corridor's direction...
        if (!firstCorridor && direction == oppositeDirection)
        {
            // Rotate the direction 90 degrees clockwise (North becomes East, East becomes South, etc).
            // This is a more broken down version of the opposite direction operation above but instead of adding 2 we're adding 1.
            // This means instead of rotating 180 (the opposite direction) we're rotating 90.
            int directionInt = direction.getValue();
            directionInt++;
            directionInt = directionInt % 4;
            direction = getDirection(directionInt);

        }

        // Set a random length.
        corridorLength = length.getRandom();

        // Create a cap for how long the length can be (this will be changed based on the direction and position).
        int maxLength = length.m_Max;

        switch (direction)
        {
            // If the choosen direction is North (up)...
            case North:
                // ... the starting position in the x axis can be random but within the width of the room.
                startXPos = RandomGenerator.random(room.xPos, room.xPos + room.roomWidth - 1);

                // The starting position in the y axis must be the top of the room.
                startYPos = room.yPos + room.roomHeight;

                // The maximum length the corridor can be is the height of the board (rows) but from the top of the room (y pos + height).
                maxLength = rows - startYPos - roomHeight.m_Min;
                break;
            case East:
                startXPos = room.xPos + room.roomWidth;
                startYPos =RandomGenerator.random(room.yPos, room.yPos + room.roomHeight - 1);
                maxLength = columns - startXPos - roomWidth.m_Min;
                break;
            case South:
                startXPos = RandomGenerator.random(room.xPos, room.xPos + room.roomWidth);
                startYPos = room.yPos;
                maxLength = startYPos - roomHeight.m_Min;
                break;
            case West:
                startXPos = room.xPos;
                startYPos = RandomGenerator.random(room.yPos, room.yPos + room.roomHeight);
                maxLength = startXPos - roomWidth.m_Min;
                break;
        }

        // We clamp the length of the corridor to make sure it doesn't go off the board.
        corridorLength = NumberUtil.clamp(corridorLength, 1, maxLength);
    }
}