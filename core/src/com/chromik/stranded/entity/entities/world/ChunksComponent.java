package com.chromik.stranded.entity.entities.world;

import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;

import java.util.ArrayList;

public class ChunksComponent extends Component {
	public Chunk[] chunks;
	public ArrayList<Chunk> loaded;


	public void init() {
		SizeComponent info = this.entity.getComponent(SizeComponent.class);

		this.chunks = new Chunk[(int) (info.width * info.height)];
		this.loaded = new ArrayList<>();
	}
}