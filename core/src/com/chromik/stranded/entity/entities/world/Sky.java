package com.chromik.stranded.entity.entities.world;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.TextureComponent;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.graphics.GameScreenOptions;
import com.chromik.stranded.graphics.Graphics;

public class Sky extends Entity {

	private int startX; // Starting position of the sky region

	public Sky() {
		super(TextureComponent.class);

		TextureComponent texture = this.getComponent(TextureComponent.class);

		texture.texture = Assets.getTexture("bg/sky");

		this.startX = texture.texture.getRegionX();
		this.setZIndex((byte) -1);
	}

	@Override
	public void render() {
		CameraComponent cam = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);

		int xStart = (int) (cam.camera.position.x - GameScreenOptions.WIDTH / 2);
		int yStart = (int) (cam.camera.position.y - GameScreenOptions.HEIGHT / 2);

		TextureComponent texture = this.getComponent(TextureComponent.class);
		ClockComponent clock = World.getInstance().getComponent(ClockComponent.class);

		int x = (Math.min(1439, clock.minute + clock.hour * 60));

		texture.texture.setRegionX(this.startX + x);
		// Render the slice O_o
		Graphics.batch.draw(texture.texture.getTexture(), xStart, yStart, 0, 0, GameScreenOptions.WIDTH, GameScreenOptions.HEIGHT, 1, 1, 0, texture.texture.getRegionX(), texture.texture.getRegionY(), 1, GameScreenOptions.HEIGHT, false, false);
	}
}