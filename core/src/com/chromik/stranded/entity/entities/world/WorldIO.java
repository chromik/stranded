package com.chromik.stranded.entity.entities.world;

import com.badlogic.gdx.files.FileHandle;
import com.chromik.stranded.core.io.FileReader;
import com.chromik.stranded.core.io.FileWriter;
import com.chromik.stranded.core.io.IO;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.SaveableComponents;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.entities.creature.SaveComponent;
import com.chromik.stranded.entity.entities.item.tile.multitile.chest.ChestRegistry;
import com.chromik.stranded.entity.entities.item.tile.multitile.door.DoorRegistry;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.entity.entities.world.chunk.ChunkIO;
import com.chromik.stranded.entity.entities.world.generator.WorldGenerator;
import com.chromik.stranded.entity.entities.world.generator.tasks.TerrainGeneratorTask;
import com.chromik.stranded.util.log.Log;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Objects;

public class WorldIO extends IO<World> {

	public static byte VERSION = 0;

	public static void write(World world) {
		String id = world.getComponent(IdComponent.class).id;
		String[] pieces = id.split(":");

		String name = pieces[0];
		String type = pieces[1];
		String root = "data/worlds/" + name + "/" + type;

		Log.info("Saving world " + name + ":" + type);

		try {
			FileWriter writer = new FileWriter(root + "/world.wld");
			HashSet<Entity> entities = Engine.getWithAllTypes(SaveComponent.class);
			entities.add(world);

			writer.writeInt32(entities.size());

			// Save entities and components
			for (Entity entity : entities) {
				if (entity != world) {
					IdComponent idComponent = entity.getComponent(IdComponent.class);
					writer.writeString(idComponent.id);
				} else {
					writer.writeString("st:world");
				}

				for (String saveable : SaveableComponents.list) {
					Component component = entity.getComponent(SaveableComponents.map.get(saveable));

					if (component != null) {
						component.write(writer);
					}
				}
			}

			writer.close();
		} catch (Exception exception) {
			exception.printStackTrace();
			Log.error("Failed to write world " + name + ":" + type);
		}

		for (Chunk chunk : world.getComponent(ChunksComponent.class).chunks) {
			if (chunk != null) {
				ChunkIO.write(root, chunk);
			}
		}
	}

	public static World load(String name, String type) {
		Log.info("Loading world " + name + ":" + type);
		createFolders(name);
		try {
			FileReader reader = new FileReader("data/worlds/" + name + "/" + type + "/world.wld");
			World world = World.getInstance(name, type);

			int count = reader.readInt32();

			for (int i = 0; i < count; i++) {
				Entity entity;
				String id = reader.readString();

				if (Objects.equals(id, "st:world")) {
					entity = world;
				} else if (Objects.equals(id, "st:chest_registry")) {
					entity = new ChestRegistry();
				} else if (Objects.equals(id, "st:door_registry")) {
					entity = new DoorRegistry();
				} else {
					entity = Assets.creatures.create(id);
				}

				for (String saveable : SaveableComponents.list) {
					Component component = entity.getComponent(SaveableComponents.map.get(saveable));

					if (component != null) {
						component.load(reader);
					}
				}

				if (entity != world) {
					Engine.addEntity(entity);
				}
			}

			reader.close();
			return world;
		} catch (FileNotFoundException exception) {
			World world = generate(name, type);
			write(world);

			return world;
		} catch (Exception exception) {
			exception.printStackTrace();
			Log.error("Failed to load world");
			return World.getInstance(name, type);
		}
	}

	public static World generate(String name, String type) {
		Log.info("Generating world " + name + ":" + type);
		World world = WorldGenerator.forType(name, type).generate();
		TerrainGeneratorTask.getInstance().run();
		
		SizeComponent size = World.getInstance().getComponent(SizeComponent.class);
		int width = (int) size.width;
		int height = (int) size.height;
		
		for (int chunkX = 0; chunkX < width; chunkX++) {
			for (int chunkY = height - 1; chunkY >= 0; chunkY--) {
				World.getInstance().getChunk(chunkX, chunkY).calculateLighting(1f);
			}
		}
		
		return world;
	}

	private static void createFolders(String name) {
		String root = "data/worlds/" + name;
		FileHandle dir = new FileHandle(root);

		if (!dir.exists()) {
			Log.debug("Creating save folders for world hub " + name);
			createFolder(root);

			for (String world : Worlds.names) {
				createFolder(root + "/" + world);
			}
		}
	}

	private static void createFolder(String name) {
		try {
			FileHandle dir = new FileHandle(name);
			dir.mkdirs();
		} catch (Exception exception) {
			Log.error("Failed to create folder " + name);
		}
	}
}