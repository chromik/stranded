package com.chromik.stranded.entity.entities.world;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.core.Debug;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.item.tile.Wall;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.util.camera.CameraHelper;

public class World extends ChunkManager {

	private static World instance;

	private World(String name, String type) {
		instance = this;
		IdComponent id = (IdComponent) this.addComponent(IdComponent.class);
		id.id = name + ":" + type;

		ClockComponent clock = (ClockComponent) this.addComponent(ClockComponent.class);
		clock.speed = 60.0f;
	}

	public static World getInstance(String... args) {
		if (instance == null) {
			if (args.length != 2) {
				throw new ExceptionInInitializerError("This method must have params for first (init) run");
			}
			instance = new World(args[0], args[1]);
		}
		return instance;
	}

	@Override
	public void render() {
		Rectangle rect = CameraHelper.getCameraRectangleInBlocks();

		for (short x = (short) rect.x; x < (short) (rect.x + rect.width); x++) {
			for (short y = (short) rect.y; y < (short) (rect.y + rect.height); y++) {
				String blockId = this.getBlock(x, y);
				Block block = null;
				int neighbors = 15;
				float light = Debug.lightEnabled ? this.getLight(x, y) : 1f;
				int data = this.getData(x, y);

				if (blockId != null) {
					block = (Block) Assets.items.get(blockId);
					neighbors = block.getNeighbors(x, y);
				}

				if (blockId == null || neighbors != 15) {
					String wallId = this.getWall(x, y);

					if (wallId != null) {
						Wall wall = (Wall) Assets.items.get(wallId);
						wall.render(x, y, data, light);
					}
				}

				if (block != null) {
					block.render(x, y, data, light, neighbors);
				}
			}
		}
	}

	public short getHighest(short x) {
		SizeComponent size = this.getComponent(SizeComponent.class);

		if (x >= size.width * Chunk.SIZE || x < 0) {
			return 0;
		}

		for (short y = (short) (size.height * Chunk.SIZE); y >= 0; y--) {
			if (this.getBlock(x, y) != null) {
				return y;
			}
		}

		return 0;
	}
}