package com.chromik.stranded.entity.entities.world.generator.tasks;

import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;

public abstract class AbstractGeneratorTask {

    int worldWidth;
    int worldHeight;
    World world;

    AbstractGeneratorTask() {

        world = World.getInstance();
        SizeComponent size = world.getComponent(SizeComponent.class);
        worldWidth = (int) size.width * Chunk.SIZE;
        worldHeight = (int) size.height * Chunk.SIZE;
    }

    public abstract void run();

    protected static int calculateNeighbors(int worldWidth, int worldHeight, boolean[][] terrain, int worldX, int worldY) {
        int neighbors = -1; // don't count self
        for (int i = worldX - 1; i < worldX + 2; ++i) {
            for (int j = worldY - 1; j < worldY + 2; ++j) {
                if (i < 0 || j < 0 || i >= worldWidth || j >= worldHeight) {
                    neighbors++;
                    continue;
                }
                if (terrain[i][j]) {
                    neighbors++;
                }
            }
        }
        return neighbors;
    }


    public int getHighest(int worldX) {
        SizeComponent size = world.getComponent(SizeComponent.class);

        if (worldX >= size.width * Chunk.SIZE || worldX < 0) {
            return 0;
        }

        int chunkX = worldX / Chunk.SIZE;
        for (int chunkY = worldHeight / Chunk.SIZE - 1; chunkY >= 0; --chunkY) {
            Chunk chunk = world.getChunk(chunkX, chunkY);
            for (int y = Chunk.SIZE - 1; y >=0; --y) {
                if (chunk.getBlock(worldX % Chunk.SIZE, y) != null) {
                    return chunkY * Chunk.SIZE + y;
                }
            }
        }

        return 0;
    }
}
