package com.chromik.stranded.entity.entities.world.generator.tasks;

import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.util.math.RandomGenerator;

public class CaveGeneratorBubbleTask extends AbstractGeneratorTask {

    private static CaveGeneratorBubbleTask instance = null;

    private CaveGeneratorBubbleTask() {
        super();
    }

    public static CaveGeneratorBubbleTask getInstance() {
        if (instance == null) {
            instance = new CaveGeneratorBubbleTask();
        }
        return instance;
    }

    @Override
    public void run() {

        boolean[][] solidMap = new boolean[worldWidth][worldHeight];

        for (int y = 0; y < worldHeight; y++) {
            for (int x = 0; x < worldWidth; x++) {
                solidMap[x][y] = RandomGenerator.randomBoolean();
            }
        }

        for (int i = 0; i < 8; i++) {
            solidMap = nextStep(worldWidth, worldHeight, solidMap);
        }


        for (int chunkX = 0; chunkX < worldWidth / Chunk.SIZE; chunkX++) {
            for (int chunkY = 0; chunkY < worldHeight / Chunk.SIZE; chunkY++) {
                Chunk chunk = world.getChunk(chunkX, chunkY);
                for (int x = 0; x < Chunk.SIZE; x++) {
                    for (int y = 0; y < Chunk.SIZE; y++) {

                        if (chunk.getBlock(x, y) == null || chunk.getBlock(x, y).equals("st:dirt")) {
                            continue;
                        }

                        if (!solidMap[chunkX * Chunk.SIZE + x][chunkY * Chunk.SIZE + y]) {
                            chunk.setBlock(null, x, y);
                            chunk.setWall(null, x, y);
                        } else {
                            int neighbors = calculateNeighbors(
                                worldWidth,
                                worldHeight,
                                solidMap,
                                chunkX * Chunk.SIZE + x,
                                chunkY * Chunk.SIZE + y
                            );

                            if (neighbors != 8) {
                                chunk.setBlock("st:grass", x, y);
                                chunk.setWall("st:grass_wall", x, y);
                            }
                        }
                        chunk.setData(TileHelper.main.create(), x, y);
                    }
                }
            }
        }
    }

    private static boolean[][] nextStep(int worldWidth, int worldHeight, boolean[][] terrain) {
        boolean[][] solidMap = new boolean[worldWidth][worldHeight];

        for (int y = 0; y < worldHeight; y++) {
            for (int x = 0; x < worldWidth; x++) {
                int neighbors = calculateNeighbors(worldWidth, worldHeight, terrain, x, y);

                if (terrain[x][y]) {
                    if (neighbors < 3) {
                        solidMap[x][y] = false;
                    } else {
                        solidMap[x][y] = true;
                    }
                } else {
                    if (neighbors > 4) {
                        solidMap[x][y] = true;
                    } else {
                        solidMap[x][y] = false;
                    }
                }
            }
        }

        return solidMap;
    }

}
