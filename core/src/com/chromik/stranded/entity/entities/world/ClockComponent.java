package com.chromik.stranded.entity.entities.world;

import com.chromik.stranded.core.io.FileReader;
import com.chromik.stranded.core.io.FileWriter;
import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.util.log.Log;

public class ClockComponent extends Component {

	public int second;
	public byte minute;
	public byte hour;
	public byte day;
	public float speed = 1.0f; // speed modifier (1.0 normal speed, 0.0 stop, 2.0 x2 speed)

	public boolean isNight() {
		return this.hour > 20 || this.hour < 4;
	}

	@Override
	public void load(FileReader reader) {
		try {
			this.second = reader.readByte();
			this.minute = reader.readByte();
			this.hour = reader.readByte();
			this.speed = reader.readFloat();
		} catch (Exception exception) {
			exception.printStackTrace();
			Log.error("Failed to load ClockComponent");
		}
	}

	@Override
	public void write(FileWriter writer) {
		try {
			writer.writeByte((byte) this.second);
			writer.writeByte((byte) this.minute);
			writer.writeByte((byte) this.hour);
			writer.writeFloat(this.speed);
		} catch (Exception exception) {
			exception.printStackTrace();
			Log.error("Failed to save ClockComponent");
		}
	}
}