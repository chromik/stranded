package com.chromik.stranded.entity.entities.world.generator;

import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.generator.generators.TerrainGenerator;
import com.chromik.stranded.entity.entities.world.generator.generators.ForestGenerator;
import com.chromik.stranded.entity.entities.world.generator.tasks.TerrainGeneratorTask;

import java.util.Objects;


public class WorldGenerator {

	protected String name;
	protected World world;

	public WorldGenerator(String name) {
		this.name = name;
	}

	public World generate() {
		this.world = World.getInstance(this.name, "forest");

		// Generate all here


		TerrainGeneratorTask.getInstance().run();

		return this.world;
	}

	public static WorldGenerator forType(String name, String type) {
		if (Objects.equals(type, "forest")) {
			return new ForestGenerator(name);
		} else if (Objects.equals(type, "flat")) {
			return new TerrainGenerator(name);
		}

		return new WorldGenerator(name);
	}

}