package com.chromik.stranded.entity.entities.world.generator.generators;

import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.entity.entities.world.generator.WorldGenerator;

public class ForestGenerator extends WorldGenerator {
	public ForestGenerator(String name) {
		super(name);
	}

	@Override
	public World generate() {
		this.world = World.getInstance(this.name, "forest");
		
		SizeComponent size = World.getInstance().getComponent(SizeComponent.class);
		int width = (int) size.width;
		int height = (int) size.height;
		
		int groundLevel = height * Chunk.SIZE / 4 * 3;
		
		for (int chunkX = 0; chunkX < width; chunkX++) {
			for (int chunkY = 0; chunkY < height; chunkY++) {
				Chunk chunk = World.getInstance().getChunk(chunkX, chunkY);
				for (int x = 0; x < Chunk.SIZE; x++) {
					int gy = groundLevel + (int)(Math.cos((chunkX * Chunk.SIZE + x) / 4.0) * 4) - chunkY * Chunk.SIZE;
					for (int y = 0; y < Chunk.SIZE; y++) {
						if (y < gy) {
							chunk.setBlock("st:dirt", x, y);
							chunk.setWall("st:dirt_wall", x, y);
						} else if (y == gy) {
							chunk.setBlock("st:grass", x, y);
							chunk.setWall("st:grass_wall", x, y);
						}

						this.world.setData(TileHelper.main.create(), x, y);
					}
				}
			}
		}

		return this.world;
	}
}