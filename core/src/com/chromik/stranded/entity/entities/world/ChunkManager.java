package com.chromik.stranded.entity.entities.world;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.entity.entities.world.chunk.ChunkIO;
import com.chromik.stranded.util.geometry.BlockVector;

public class ChunkManager extends Entity {
	public ChunkManager() {
		super(SizeComponent.class);

		SizeComponent size = this.getComponent(SizeComponent.class);
		size.width = 200;
		size.height = 50;
		
		addComponent(ChunksComponent.class);
	}

	public String getBlock(BlockVector vector) {
		if (this.isOut(vector.x, vector.y)) {
			return null;
		}

		Chunk chunk = this.getChunkfor (vector.x, vector.y);

		if (chunk == null) {
			return null;
		}
		return chunk.getBlock(chunk.toRelativeX(vector.x), chunk.toRelativeY(vector.y));
	}

	public String getBlock(int x, int y) {
		if (this.isOut(x, y)) {
			return null;
		}

		Chunk chunk = this.getChunkfor (x, y);

		if (chunk == null) {
			return null;
		}
		return chunk.getBlock(chunk.toRelativeX(x), chunk.toRelativeY(y));
	}

	public void setBlock(String value, BlockVector vector) {
		setBlock(value, vector.x, vector.y);
	}

	public void setBlock(String value, int x, int y) {
		if (this.isOut(x, y)) {
			return;
		}
		
		Chunk chunk = this.getChunkfor (x, y);
		
		if (chunk != null) {
			chunk.setBlock(value, chunk.toRelativeX(x), chunk.toRelativeY(y));
			Engine.sendMessage(SystemMessage.Type.TILE_UPDATE.create()
					.set("action", "set")
					.set("value", value)
					.set("x", x)
					.set("y", y));
		}
	}

	public String getWall(int x, int y) {
		if (this.isOut(x, y)) {
			return null;
		}

		Chunk chunk = this.getChunkfor (x, y);

		if (chunk == null) {
			return null;
		}
		return chunk.getWall(chunk.toRelativeX(x), chunk.toRelativeY(y));
	}

	public void setWall(String value, int x, int y) {
		if (this.isOut(x, y)) {
			return;
		}

		Chunk chunk = this.getChunkfor (x, y);

		if (chunk != null) {
			chunk.setWall(value, chunk.toRelativeX(x), chunk.toRelativeY(y));
			Engine.sendMessage(SystemMessage.Type.WALL_UPDATE.create()
					.set("action", "set")
					.set("value", value)
					.set("x", x)
					.set("y", y));
		}
	}

	public float getLight(int x, int y) {
		if (isOut(x, y)) {
			return 0.0f;
		}

		Chunk chunk = this.getChunkfor (x, y);

		if (chunk == null) {
			return 0.0f;
		} else {
			return chunk.getLight(chunk.toRelativeX(x), chunk.toRelativeY(y));
		}
	}

	public void setLight(float value, int x, int y) {
		if (isOut(x, y)) {
			return;
		}

		Chunk chunk = this.getChunkfor (x, y);

		if (chunk != null) {
			chunk.setLight(value, chunk.toRelativeX(x), chunk.toRelativeY(y));
		}
	}

	public int getData(BlockVector vector) {
		return  getData(vector.x, vector.y);
	}

	public int getData(int x, int y) {
		if (this.isOut(x, y)) {
			return 0;
		}

		Chunk chunk = this.getChunkfor (x, y);

		if (chunk == null) {
			return 0;
		}
		return chunk.getData(chunk.toRelativeX(x), chunk.toRelativeY(y));
	}

	public void setData(int value, BlockVector vector) {
		setData(value, vector.x, vector.y);
	}

	public void setData(int value, int x, int y) {
		if (this.isOut(x, y)) {
			return;
		}

		Chunk chunk = this.getChunkfor (x, y);

		if (chunk != null) {
			chunk.setData(value, chunk.toRelativeX(x), chunk.toRelativeY(y));
		}
	}
	public Chunk getChunkfor (int x, int y) {
		return getChunk(x / Chunk.SIZE, y / Chunk.SIZE);
	}
	
	public Chunk getChunk(int x, int y) {
		SizeComponent size = getComponent(SizeComponent.class);
		if (x < 0 || y < 0 || x >= size.width || y >= size.height) {
			return null;
		}
		
		int index = getChunkIndex(x, y);
		ChunksComponent chunks = this.getComponent(ChunksComponent.class);
		
		if (chunks.chunks[index] == null) {
			loadChunk(x, y);
			Chunk chunk = chunks.chunks[index];
			Engine.sendMessage(SystemMessage.Type.LOAD_CHUNK.create()
					.set("chunk", chunk)
					.set("x", x)
					.set("y", y));
			return chunk;
		}
		
		return chunks.chunks[index];
	}

	private int getChunkIndex(int x, int y) {
		return x + y * (int)this.getComponent(SizeComponent.class).width;
	}


	protected boolean isOut(int x, int y) {
		SizeComponent size = this.getComponent(SizeComponent.class);
		return x < 0 || y < 0 || x > size.width * Chunk.SIZE - 1 || y > size.height * Chunk.SIZE -1;
	}

	protected void loadChunk(int x, int y) {
		String id = "data/worlds/" + this.getComponent(IdComponent.class).id.replace(':', '/');

		ChunksComponent chunks = this.getComponent(ChunksComponent.class);
		Chunk chunk = ChunkIO.load(id, x, y);

		chunks.loaded.add(chunk);
		chunks.chunks[this.getChunkIndex(x, y)] = chunk;

		Engine.addEntity(chunk);
	}
}