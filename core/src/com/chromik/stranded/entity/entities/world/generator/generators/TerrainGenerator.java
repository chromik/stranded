package com.chromik.stranded.entity.entities.world.generator.generators;

import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.generator.WorldGenerator;
import com.chromik.stranded.entity.entities.world.generator.tasks.CaveGeneratorBubbleTask;
import com.chromik.stranded.entity.entities.world.generator.tasks.CaveGeneratorSimplexTask;
import com.chromik.stranded.entity.entities.world.generator.tasks.TerrainGeneratorTask;

public class TerrainGenerator extends WorldGenerator {
    public TerrainGenerator(String name) {
        super(name);
    }

    @Override
    public World generate() {
        this.world = World.getInstance(this.name, "flat");
		TerrainGeneratorTask.getInstance().run();
        CaveGeneratorSimplexTask.getInstance().run();
        CaveGeneratorBubbleTask.getInstance().run();
        return this.world;
    }
}
