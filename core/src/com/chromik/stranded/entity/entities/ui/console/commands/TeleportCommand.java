package com.chromik.stranded.entity.entities.ui.console.commands;

import com.chromik.stranded.entity.Players;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.ui.console.ConsoleCommand;
import com.chromik.stranded.entity.entities.ui.console.UiConsole;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;


public class TeleportCommand extends ConsoleCommand {
    public TeleportCommand() {
        super("teleport", "Teleport player to given x y");
    }

    @Override
    public void run(UiConsole console, String[] args) {
        World world = World.getInstance();


        PositionComponent player = Players.clientPlayer.getComponent(PositionComponent.class);
        SizeComponent worldSize = world.getComponent(SizeComponent.class);

        try {
            int x = Integer.valueOf(args[0]);
            int y = Integer.valueOf(args[1]);
                player.x = x * Block.SIZE;
                player.y = y * Block.SIZE;
        } catch (Exception ex) {
            console.print("Failed to parse args");
            ex.printStackTrace();
        }

    }
}
