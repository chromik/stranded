package com.chromik.stranded.entity.entities.ui.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.chromik.stranded.entity.Players;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.TextureComponent;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.ItemUseComponent;
import com.chromik.stranded.entity.entities.item.inventory.InventoryComponent;
import com.chromik.stranded.entity.entities.item.inventory.ItemComponent;
import com.chromik.stranded.entity.entities.ui.UiElement;
import com.chromik.stranded.graphics.Graphics;
import com.chromik.stranded.util.collision.Collider;
import com.chromik.stranded.util.graphics.OutlinePrinter;
import com.chromik.stranded.util.input.Input;
import com.chromik.stranded.util.input.SimpleInputProcessor;


public class UiInventory extends UiElement implements SimpleInputProcessor {

	protected static TextureRegion slotTexture;
	protected static GlyphLayout layout;

	public UiInventory(Rectangle rect, InventoryComponent inventory) {
		super(rect);

		if (slotTexture == null) {
			slotTexture = Assets.getTexture("ui/inventory_slot");
		}

		if (layout == null) {
			layout = new GlyphLayout();
		}

		this.components.put(InventoryComponent.class, inventory);
		Input.multiplexer.addProcessor(this);
	}

	@Override
	public void renderUi() {
		if (Gdx.input.isButtonPressed(0)) {
			InventoryComponent inventory = this.getComponent(InventoryComponent.class);
			ItemComponent slot = inventory.inventory[inventory.selectedSlot];

			if (!slot.isEmpty()) {
				ItemUseComponent use = slot.item.getComponent(ItemUseComponent.class);

				if (use.autoUse && slot.item.use(inventory.getEntity())) {
					slot.count -= 1;

					if (slot.count == 0) {
						slot.item = null;
					}
				}
			}
		}

		PositionComponent position = this.getComponent(PositionComponent.class);
		InventoryComponent inventory = this.getComponent(InventoryComponent.class);

		for (int i = 0; i < (inventory.open ? inventory.inventory.length : Math.min(inventory.inventory.length, 10)); i++) {
			this.renderSlot(inventory, position.x + i % 10 * 25, (float) (position.y + Math.floor(i / 10) * 25), i, inventory.inventory[i]);
		}

		ItemComponent item = Players.clientPlayer.getComponent(ItemComponent.class);

		if (!item.isEmpty()) {
			Vector3 mouse = CameraSystem.getInstance().get("ui").getComponent(CameraComponent.class).camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
			Graphics.batch.draw(item.item.getComponent(TextureComponent.class).texture, mouse.x + 8, mouse.y - 8);


			if (item.count > 1) {
				String str = String.valueOf(item.count);
				layout.setText(Assets.f4, str);

				OutlinePrinter.print(Assets.f4, str, (int) (mouse.x + 22 - layout.width), (int) mouse.y - 8);
			}
		}
	}

	protected void renderSlot(InventoryComponent inventory, float x, float y, int index, ItemComponent item) {
		if (inventory.selectedSlot == index) {
			Graphics.batch.setColor(1.0f, 1.0f, 0.7f, 1.0f);
		}

		Graphics.batch.draw(slotTexture, x, y);

		if (inventory.selectedSlot == index) {
			Graphics.batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		}

		if (!item.isEmpty()) {
			TextureComponent texture = item.item.getComponent(TextureComponent.class);
			TextureRegion region = texture.texture;

			// draw the texture at the center of the slot
			Graphics.batch.draw(region, x + (24 - region.getRegionWidth()) / 2, y + (24 - region.getRegionHeight()) / 2);

			if (item.count > 1) {
				String str = String.valueOf(item.count);
				layout.setText(Assets.f4, str);

				OutlinePrinter.print(Assets.f4, str, (int) (x + 22 - layout.width), (int) (y + 8));
			}
		}

		if (index < 10) {
			OutlinePrinter.print(Assets.f4, String.valueOf(index < 9 ? index + 1 : 0), (int) (x + 3), (int) (y + 21));
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		if (!Input.blocked && keycode == Assets.keys.get("toggle_inventory")) {
			InventoryComponent inventory = this.getComponent(InventoryComponent.class);
			inventory.open = !inventory.open;
		} else if (!Input.blocked && keycode == Assets.keys.get("num0")) { // NUM0
			setActiveItemSlot((short)9);
		}else if (!Input.blocked && keycode == Assets.keys.get("num1")) { // NUM1
			setActiveItemSlot((short)0);
		}else if (!Input.blocked && keycode == Assets.keys.get("num2")) { // NUM2
			setActiveItemSlot((short)1);
		}else if (!Input.blocked && keycode == Assets.keys.get("num3")) { // NUM3
			setActiveItemSlot((short)2);
		}else if (!Input.blocked && keycode == Assets.keys.get("num4")) { // NUM4
			setActiveItemSlot((short)3);
		}else if (!Input.blocked && keycode == Assets.keys.get("num5")) { // NUM5
			setActiveItemSlot((short)4);
		}else if (!Input.blocked && keycode == Assets.keys.get("num6")) { // NUM6
			setActiveItemSlot((short)5);
		}else if (!Input.blocked && keycode == Assets.keys.get("num7")) { // NUM7
			setActiveItemSlot((short)6);
		}else if (!Input.blocked && keycode == Assets.keys.get("num8")) { // NUM8
			setActiveItemSlot((short)7);
		}else if (!Input.blocked && keycode == Assets.keys.get("num9")) { // NUM9
			setActiveItemSlot((short)8);
		}



		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (button == 0) { // TODO: support multiple buttons
			PositionComponent position = this.getComponent(PositionComponent.class);
			InventoryComponent inventory = this.getComponent(InventoryComponent.class);

			if (inventory.open) {
				Vector3 mouse = CameraSystem.getInstance().get("ui").getComponent(CameraComponent.class).camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));

				for (int i = 0; i < inventory.inventory.length; i++) {
					float x = position.x + i % 10 * 25;
					float y = (float) (position.y + Math.floor(i / 10) * 25);

					Rectangle object = new Rectangle(x, y, 24, 24);
					Rectangle cursor = new Rectangle(mouse.x, mouse.y, 1, 1);

					if (Collider.hitTest(object, cursor)) {
						ItemComponent item = inventory.inventory[i];
						ItemComponent self = Players.clientPlayer.getComponent(ItemComponent.class);

						ItemComponent tmp = new ItemComponent();

						tmp.item = self.item;
						tmp.count = self.count;

						self.item = item.item;
						self.count = item.count;

						item.item = tmp.item;
						item.count = tmp.count;

						return true;
					}
				}
			}

			ItemComponent slot = inventory.inventory[inventory.selectedSlot];

			if (!slot.isEmpty()) {
				ItemUseComponent use = slot.item.getComponent(ItemUseComponent.class);

				if (!use.autoUse && slot.item.use(inventory.getEntity())) {
					slot.count -= 1;

					if (slot.count == 0) {
						slot.item = null;
					}

					return true;
				}
			}
		}

		return false;
	}

	private void setActiveItemSlot(short index) {
		InventoryComponent inventory = this.getComponent(InventoryComponent.class);
		inventory.selectedSlot = index;
	}

	@Override
	public boolean scrolled(int amount) {
		InventoryComponent inventory = this.getComponent(InventoryComponent.class);
		inventory.selectedSlot = (short) ((inventory.selectedSlot + amount) % 10);

		if (inventory.selectedSlot < 0) {
			inventory.selectedSlot += 10;
		}

		return false;
	}
}