package com.chromik.stranded.entity.entities.ui.console.commands;

import com.chromik.stranded.entity.entities.ui.console.ConsoleCommand;
import com.chromik.stranded.entity.entities.ui.console.UiConsole;
import com.chromik.stranded.entity.entities.world.ClockComponent;
import com.chromik.stranded.entity.entities.world.World;

public class NightCommand extends ConsoleCommand {
	public NightCommand() {
		super("night", "Sets time to night");
	}

	@Override
	public void run(UiConsole console, String[] args) {
		ClockComponent clock = World.getInstance().getComponent(ClockComponent.class);

		clock.hour = 22;
		clock.minute = 0;
	}
}