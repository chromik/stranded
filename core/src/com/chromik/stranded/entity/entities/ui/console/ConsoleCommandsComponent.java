package com.chromik.stranded.entity.entities.ui.console;

import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.entity.entities.ui.console.commands.*;

import java.util.ArrayList;

public class ConsoleCommandsComponent extends Component {

	public ArrayList<ConsoleCommand> commands = new ArrayList<>();

	public ConsoleCommandsComponent() {
		this.commands.add(new HelpCommand());
		this.commands.add(new GiveCommand());
		this.commands.add(new DayCommand());
		this.commands.add(new NightCommand());
		this.commands.add(new SpawnCommand());
		this.commands.add(new InfoCommand());
		this.commands.add(new LightCommand());
		this.commands.add(new TeleportCommand());
	}
}