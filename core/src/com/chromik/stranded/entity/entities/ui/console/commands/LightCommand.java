package com.chromik.stranded.entity.entities.ui.console.commands;

import com.chromik.stranded.core.Debug;
import com.chromik.stranded.entity.entities.ui.console.ConsoleCommand;
import com.chromik.stranded.entity.entities.ui.console.UiConsole;

public class LightCommand extends ConsoleCommand {
    public LightCommand() {
        super("light", "Sets light on/off");
    }

    @Override
    public void run(UiConsole console, String[] args) {

        boolean paramParsed = false;
        if (args.length == 1) {
            String param = args[0];
            paramParsed = tryToParseTrueParam(param);
            if (paramParsed) { // IS ENABLE?
                 Debug.lightEnabled = true;

            } else {
                paramParsed = tryToParseFalseParam(param);
                if (paramParsed) { // IS DISABLE?
                    Debug.lightEnabled = false;
                }
            }
        }

        if (args.length == 0) { // IS WITHOUT PARAM (SWITCH) ?
            Debug.lightEnabled = !Debug.lightEnabled;

        } else if (!paramParsed) { // INVALID PARAM
            console.print("Invalid parameter");
            return;
        }

        console.print("Light " + (Debug.lightEnabled ? "ON" : "OFF"));
    }
}
