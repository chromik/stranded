package com.chromik.stranded.entity.entities.ui.console;

import com.chromik.stranded.entity.component.Component;


public class ConsoleLine extends Component {
	public static float EXPIRE_TIME = 1000.0f;
	public String line = "";
	public float time = 0.0f;
}