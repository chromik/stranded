package com.chromik.stranded.entity.entities.ui.console.commands;

import com.chromik.stranded.entity.Players;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.ui.console.ConsoleCommand;
import com.chromik.stranded.entity.entities.ui.console.UiConsole;
import com.chromik.stranded.entity.entities.world.ChunksComponent;
import com.chromik.stranded.entity.entities.world.ClockComponent;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;

public class InfoCommand extends ConsoleCommand {
    public InfoCommand() {
        super("info", "Available info");
    }

    @Override
    public void run(UiConsole console, String[] args) {
        World world = World.getInstance();


        ClockComponent clock = world.getComponent(ClockComponent.class);
        console.print("TIME");
        console.print("Day: " + clock.day);
        console.print(clock.hour + ":" + clock.minute + ":" + clock.second + (clock.isNight() ? " (night)" : " (day)"));
        console.print("");


        PositionComponent player = Players.clientPlayer.getComponent(PositionComponent.class);
        SizeComponent worldSize = world.getComponent(SizeComponent.class);

        int playerX = Math.round(player.x / Block.SIZE);
        int playerY = Math.round(player.y / Block.SIZE);


        ChunksComponent chunks = World.getInstance().getComponent(ChunksComponent.class);
        console.print(chunks.loaded.size() + "/" + chunks.chunks.length + " chunks are loaded");


        console.print("Player world pixel position");
        console.print("x: " + playerX * Block.SIZE + "/" + (worldSize.width * Chunk.SIZE - 1) * Block.SIZE);
        console.print("y: " + playerY * Block.SIZE + "/" + (worldSize.height * Chunk.SIZE - 1) * Block.SIZE);
        console.print("");

        console.print("Player world block position");
        console.print("x: " + playerX + "/" + (worldSize.width * Chunk.SIZE - 1));
        console.print("y: " + playerY + "/" + (worldSize.height * Chunk.SIZE - 1));
        console.print("");


        console.print("Player chunk position");

        console.print("x: " + playerX / Chunk.SIZE + "/" + (worldSize.width - 1));
        console.print("y: " + playerY / Chunk.SIZE + "/" +(worldSize.height - 1));
        console.print("");

        console.print("Player block in chuck position");
        console.print("x: " + playerX % Chunk.SIZE + "/" + (Chunk.SIZE - 1));
        console.print("y: " + playerY % Chunk.SIZE + "/" + (Chunk.SIZE - 1));
        console.print("");

    }
}
