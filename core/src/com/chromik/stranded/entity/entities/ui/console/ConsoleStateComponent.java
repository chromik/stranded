package com.chromik.stranded.entity.entities.ui.console;

import com.chromik.stranded.entity.component.Component;


public class ConsoleStateComponent extends Component {
	public String input = "";
	public boolean open = false;
}