package com.chromik.stranded.entity.entities.ui;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.component.TextureComponent;
import com.chromik.stranded.graphics.Graphics;

public class UiElement extends Entity {
	public UiElement(Rectangle rect, Class<? extends Component> ... components) {
		super(PositionComponent.class, SizeComponent.class, UiComponent.class);

		this.setZIndex((byte) 10);
		this.addComponent(components);

		PositionComponent position = this.getComponent(PositionComponent.class);

		position.x = rect.x;
		position.y = rect.y;

		SizeComponent size = this.getComponent(SizeComponent.class);

		size.width = rect.width;
		size.height = rect.height;
	}

	protected void renderNinePartTexture() {
		TextureComponent texture = this.getComponent(TextureComponent.class);

		// No texture, no work
		if (texture == null) {
			return;
		}

		PositionComponent position = this.getComponent(PositionComponent.class);
		SizeComponent size = this.getComponent(SizeComponent.class);

		// TODO: sliced sprites
		// for hint look up this: https://docs.unity3d.com/Manual/9SliceSprites.html

		Graphics.batch.draw(texture.texture, position.x, position.y, size.width, size.height);
	}
}