package com.chromik.stranded.entity.entities.ui.console;

import com.chromik.stranded.entity.component.Component;

import java.util.ArrayList;


public class ConsoleLinesComponent extends Component {

	public ArrayList<ConsoleLine> lines = new ArrayList<>();
}