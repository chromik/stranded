package com.chromik.stranded.entity.entities.ui.window;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.TextureComponent;
import com.chromik.stranded.entity.entities.ui.UiElement;

public class UiWindow extends UiElement {
	public UiWindow(Rectangle rect) {
		super(rect, IdComponent.class);

		TextureComponent texture = (TextureComponent) this.addComponent(TextureComponent.class);
		texture.texture = Assets.getTexture("ui/bg");
	}

	@Override
	public void renderUi() {
		this.renderNinePartTexture();
	}
}