package com.chromik.stranded.entity.entities.ui.console;

import java.util.Arrays;

public class ConsoleCommand {

	private String name;
	private String description;

	public ConsoleCommand(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public void run(UiConsole console, String[] args) {
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	protected boolean tryToParseTrueParam(String noCandidate) {
		for (String yes : Arrays.asList("yes", "on", "1", "true")) {
			if (noCandidate.toLowerCase().equalsIgnoreCase(yes)) {
				return true;
			}
		}
		return false;
	}

	protected boolean tryToParseFalseParam(String noCandidate) {
		for (String no : Arrays.asList("no", "off", "0", "false")) {
			if (noCandidate.toLowerCase().equalsIgnoreCase(no)) {
				return true;
			}
		}
		return false;
	}
}