package com.chromik.stranded.entity.entities.item.inventory;

import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.entity.entities.item.Item;

public class ItemComponent extends Component {
	public Item item;
	public short count = 0;

	public boolean isEmpty() {
		return this.item == null;
	}
}