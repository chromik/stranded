package com.chromik.stranded.entity.entities.item.tile;

import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.World;

public class Wall extends Tile { // background block
	public Wall(String id) {
		super(id);
	}

	@Override
	public void loadFields(JsonValue asset) {
		super.loadFields(asset);

		this.getComponent(TileComponent.class).tiles =
			Assets.getTexture("walls/" + this.getComponent(IdComponent.class).id.replace(':', '_')
				+ "_tiles").split(Tile.SIZE, Tile.SIZE);
	}

	@Override
	protected void place(short x, short y) {
		World.getInstance().setWall(this.getComponent(IdComponent.class).id, x, y);
		World.getInstance().setData(TileHelper.main.create(), x, y);
	}

	protected String getNeighbor(short x, short y) {
		return World.getInstance().getWall(x, y);
	}
}