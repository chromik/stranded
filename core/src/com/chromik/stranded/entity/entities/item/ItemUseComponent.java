package com.chromik.stranded.entity.entities.item;

import com.chromik.stranded.entity.component.Component;

public class ItemUseComponent extends Component {
	public float useTime = 0.1f; // how long is item used
	public float currentTime = 0.0f;
	public boolean autoUse = false;
}