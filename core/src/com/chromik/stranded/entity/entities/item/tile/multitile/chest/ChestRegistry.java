package com.chromik.stranded.entity.entities.item.tile.multitile.chest;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.entities.creature.SaveComponent;

public class ChestRegistry extends Entity {
	public ChestRegistry() {
		super(ChestComponent.class, PositionComponent.class, SaveComponent.class, IdComponent.class);

		IdComponent id = this.getComponent(IdComponent.class);

		id.id = "st:chest_registry";
	}
}