package com.chromik.stranded.entity.entities.item.tile.interactable;

public interface Interactable {

	default void onClick(int x, int y) {
	}

	default boolean checkOverlap(int x, int y) {
		return false;
	}
}