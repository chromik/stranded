package com.chromik.stranded.entity.entities.item.tile.interactable;

import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.entities.item.tile.Block;

public class InteractableBlock extends Block implements Interactable {
	public InteractableBlock(String id) {
		super(id);

		this.addComponent(InteractionComponent.class);
		Engine.addEntity(this);
	}
}