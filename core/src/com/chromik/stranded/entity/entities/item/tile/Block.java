package com.chromik.stranded.entity.entities.item.tile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.SolidComponent;
import com.chromik.stranded.entity.component.physics.CollisionComponent;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.World;


public class Block extends Tile {

	public static short MAX_LIGHT = 16;

	public Block(String id) {
		super(id);

		this.addComponent(SolidComponent.class);
		this.addComponent(CollisionComponent.class);
	}

	@Override
	protected boolean canBeUsed() {
		CameraComponent camera = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);
		Vector3 mouse = camera.camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));

		mouse.x /= SIZE;
		mouse.y /= SIZE;

		return (World.getInstance().getBlock((short) mouse.x, (short) mouse.y) == null
			&& (World.getInstance().getBlock((short) (mouse.x - 1), (short) mouse.y) != null
			|| World.getInstance().getBlock((short) (mouse.x + 1), (short) mouse.y) != null
			|| World.getInstance().getBlock((short) mouse.x, (short) (mouse.y - 1)) != null
			|| World.getInstance().getBlock((short) mouse.x, (short) (mouse.y + 1)) != null));
	}

	@Override
	public void loadFields(JsonValue asset) {
		super.loadFields(asset);

		this.getComponent(TileComponent.class).tiles =
			Assets.getTexture("blocks/" + this.getComponent(IdComponent.class).id.replace(':', '_')
				+ "_tiles").split(Tile.SIZE, Tile.SIZE);

		this.getComponent(SolidComponent.class).solid =
			asset.getBoolean("solid", true);
	}

	public boolean isEmitter() {
		return false;
	}


	@Override
	protected void place(short x, short y) {
		World.getInstance().setBlock(this.getComponent(IdComponent.class).id, x, y);
		World.getInstance().setData(TileHelper.main.create(), x, y);
	}

	protected String getNeighbor(short x, short y) {
		return World.getInstance().getBlock(x, y);
	}
}