package com.chromik.stranded.entity.entities.item.tile;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chromik.stranded.entity.component.Component;

public class TileComponent extends Component {
	public TextureRegion[][] tiles;
}