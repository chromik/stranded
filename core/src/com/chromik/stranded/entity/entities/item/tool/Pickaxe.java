package com.chromik.stranded.entity.entities.item.tool;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.Players;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.Item;
import com.chromik.stranded.entity.entities.item.ItemUseComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.sounds.SoundUtil;
import com.chromik.stranded.util.geometry.BlockVector;

import static com.chromik.stranded.util.geometry.GeometryUtil.calculateDistance;
import static com.chromik.stranded.util.geometry.GeometryUtil.convertToBlockVector;

public class Pickaxe extends Item {
	public Pickaxe(String id) {
		super(id);
		this.getComponent(ItemUseComponent.class).autoUse = true;
	}

	@Override
	protected boolean onUse(Entity entity) {
		CameraComponent camera = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);
		Vector3 mouse = camera.camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));


		BlockVector cursorTile = convertToBlockVector(mouse.x, mouse.y);

		String id = World.getInstance().getBlock(cursorTile);

		if (id != null) {
			Block block = (Block) Assets.items.get(id);

			int data = World.getInstance().getData(cursorTile);
			int health = block.getHealth(data);

			health -= 1;

			if (health == 0) {
				SoundUtil.playPicked();
				World.getInstance().setBlock(null, cursorTile);
				World.getInstance().setData(TileHelper.main.create(), cursorTile);
			} else {
				SoundUtil.playStoneHit();
				World.getInstance().setData(block.getHelper().setBlockHealth(data, health), cursorTile);
			}
		}

		return false;
	}

	@Override
	protected boolean canBeUsed() {

		CameraComponent camera = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);
		Vector3 mousePosition = camera.camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));

		PositionComponent playerPosition = Players.clientPlayer.getComponent(PositionComponent.class);
		SizeComponent playerSize = Players.clientPlayer.getComponent(SizeComponent.class);

		Vector2 playerCenter = new Vector2();
		playerCenter.x = playerPosition.x + playerSize.width / 2;
		playerCenter.y = playerPosition.y + playerSize.height / 2;

		BlockVector playerCenterTile = convertToBlockVector(playerCenter.x, playerCenter.y);
		BlockVector cursorTile = convertToBlockVector(mousePosition.x, mousePosition.y);

		double distance = calculateDistance(playerCenterTile, cursorTile);


		return distance < 3.5 && World.getInstance().getBlock(cursorTile) != null;
	}
}