package com.chromik.stranded.entity.entities.item.tile.multitile.door;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.entities.creature.SaveComponent;

public class DoorRegistry extends Entity {
    public DoorRegistry() {
        super(DoorComponent.class, PositionComponent.class, SaveComponent.class, IdComponent.class);

        IdComponent id = this.getComponent(IdComponent.class);
        id.id = "st:chest_registry";
    }
}