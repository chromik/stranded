package com.chromik.stranded.entity.entities.item.tile.multitile.door;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.Players;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.system.systems.ChestSystem;
import com.chromik.stranded.entity.engine.system.systems.UiSystem;
import com.chromik.stranded.entity.entities.item.inventory.InventoryComponent;
import com.chromik.stranded.entity.entities.item.tile.interactable.Interactable;
import com.chromik.stranded.entity.entities.item.tile.multitile.MultitileBlock;
import com.chromik.stranded.entity.entities.item.tile.multitile.chest.ChestComponent;
import com.chromik.stranded.entity.entities.item.tile.multitile.chest.ChestRegistry;
import com.chromik.stranded.entity.entities.ui.inventory.UiChestInventory;

public class Door extends MultitileBlock implements Interactable {
    public Door(String id) {
        super(id);
    }

    @Override
    public void onClick(int x, int y) {
        short xx = (short) (x / 8);
        short yy = (short) (y / 8);

        ChestRegistry chest = ChestSystem.instance.find(xx, yy);

        if (chest == null) {
            return; // Did not find it
        }


        UiChestInventory ui = (UiChestInventory) UiSystem.getInstance().get("chest");

        if (ui != null) {
            Engine.removeEntity(ui);
        } else {
            ChestComponent inventory = chest.getComponent(ChestComponent.class);
            Engine.addEntity(new UiChestInventory(new Rectangle(5, 106, 10, 10), inventory));
            Players.clientPlayer.getComponent(InventoryComponent.class).open = true;
        }
    }

    @Override
    protected void place(short x, short y) {
        super.place(x, y);

        // Add the entity

        ChestRegistry chest = new ChestRegistry();
        PositionComponent position = chest.getComponent(PositionComponent.class);

        position.x = x;
        position.y = y;

        Engine.addEntity(chest);
    }
}