package com.chromik.stranded.entity.entities.item.inventory;

import com.chromik.stranded.core.io.FileReader;
import com.chromik.stranded.core.io.FileWriter;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.entities.item.StackComponent;

import java.io.IOException;

public class InventoryComponent extends Component {

  protected short size = 40;

  public ItemComponent[] inventory;

  public boolean open = false;

  public short selectedSlot = 0;

  public InventoryComponent() {
    this.initSlots();
  }

  public boolean add(ItemComponent item) {
    if (item == null) {
      return false;
    }

    // Check for existing stack
    for (ItemComponent slot : this.inventory) {
      // The items match
      if (slot.item == item.item) {
        StackComponent slotMax = slot.item.getComponent(StackComponent.class);

        // If have enough space
        if (slotMax.max >= slot.count + item.count) {
          slot.count += item.count;
          return true;
        }
      }
    }

    // Did not find it, check for an empty one
    for (ItemComponent slot : this.inventory) {
      if (slot.isEmpty()) {
        slot.item = item.item;
        // Cap the value
        StackComponent slotMax = slot.item.getComponent(StackComponent.class);
        slot.count = (short) Math.min(slotMax.max, item.count);

        return true;
      }
    }

    return false;
  }

  @Override
  public void write(FileWriter writer) {
    try {
      writer.writeInt16(this.size);

      for (short i = 0; i < this.size; i++) {
        ItemComponent item = this.inventory[i];

        if (item.isEmpty()) {
          writer.writeBoolean(false);
        } else {
          writer.writeBoolean(true);

          writer.writeString(item.item.getComponent(IdComponent.class).id);
          writer.writeInt16(item.count);
        }
      }
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }

  @Override
  public void load(FileReader reader) {
    try {
      this.size = reader.readInt16();
      this.initSlots();

      for (short i = 0; i < this.size; i++) {
        if (reader.readBoolean()) {
          this.inventory[i].item = Assets.items.get(reader.readString());
          this.inventory[i].count = reader.readInt16();
        }
      }
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }

  private void initSlots() {
    this.inventory = new ItemComponent[this.size];

    for (int i = 0; i < this.size; i++) {
      this.inventory[i] = new ItemComponent();
    }
  }

  public short getSize() {
    return this.size;
  }
}