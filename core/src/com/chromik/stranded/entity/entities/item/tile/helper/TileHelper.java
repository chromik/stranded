package com.chromik.stranded.entity.entities.item.tile.helper;

import com.chromik.stranded.util.binary.BitHelper;
import com.chromik.stranded.util.math.RandomGenerator;

public class TileHelper {

	public static TileHelper main = new TileHelper();
	public static MultitileHelper multitile = new MultitileHelper();

	public int create() {
		int data = 0;

		int[] variantProbabilities = {20, 70, 10};
		data = this.setVariant(data, variantProbabilities);
		data = this.setBlockHealth(data, 3);
		data = this.setWallHealth(data, 3);

		return data;
	}

	public int setVariant(int data, int[] probabilities) {
		int probabilitiesSum = 0;
		for (int prob : probabilities) {
			probabilitiesSum += prob;
		}

		int randomValue = RandomGenerator.random(0, probabilitiesSum);

		int variant = randomValue > 0 ? -1 : 0;

		while (randomValue > 0) {
			++variant;
			randomValue -= probabilities[variant];
		}

		return BitHelper.putNumber(data, 0, 2, variant);
	}

	public int setVariant(int data, int variant) {
		return BitHelper.putNumber(data, 0, 2, variant);
	}

	public int getVariant(int data) {
		return BitHelper.getNumber(data, 0, 2);
	}

	public int setBlockHealth(int data, int health) {
		return BitHelper.putNumber(data, 2, 2, health);
	}

	public int getBlockHealth(int data) {
		return BitHelper.getNumber(data, 2, 2);
	}

	public int setWallHealth(int data, int health) {
		return BitHelper.putNumber(data, 4, 2, health);
	}

	public int getWallHealth(int data) {
		return BitHelper.getNumber(data, 4, 2);
	}
}