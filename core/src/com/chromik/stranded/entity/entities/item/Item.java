package com.chromik.stranded.entity.entities.item;

import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.DescriptionComponent;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.TextureComponent;
import com.chromik.stranded.util.log.Log;

public class Item extends Entity {
	public Item(String id) {
		super(IdComponent.class, DescriptionComponent.class, TextureComponent.class, ItemUseComponent.class,
			StackComponent.class);

		this.getComponent(IdComponent.class).id = id;
	}

	public void loadFields(JsonValue asset) {
		TextureComponent texture = this.getComponent(TextureComponent.class);
		texture.texture = Assets.getTexture("icons/" + asset.name().replace(':', '_'));

		if (texture.texture == null) {
			Log.warning("Failed to load texture for " + asset.name());
			texture.texture = Assets.getTexture("missing_texture");
		}
	}

	public void update(float delta) {
		ItemUseComponent data = this.getComponent(ItemUseComponent.class);

		if (data.currentTime > 0.0f) {
			data.currentTime -= delta;

			if (data.currentTime < 0.0f) {
				data.currentTime = 0.0f;
			}
		}
	}

	public boolean use(Entity entity) {
		ItemUseComponent data = this.getComponent(ItemUseComponent.class);

		if (data.currentTime == 0.0f && this.canBeUsed()) {
			data.currentTime = data.useTime;
			return this.onUse(entity);
		}

		return false;
	}

	protected boolean canBeUsed() {
		return true;
	}

	protected boolean onUse(Entity entity) {
		return false;
	}
}