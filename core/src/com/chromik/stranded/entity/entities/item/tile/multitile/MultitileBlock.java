package com.chromik.stranded.entity.entities.item.tile.multitile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.item.tile.helper.MultitileHelper;
import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.entity.entities.world.World;

public class MultitileBlock extends Block {
	public MultitileBlock(String id) {
		super(id);

		this.addComponent(MultitileSizeComponent.class);
	}

	@Override
	public void loadFields(JsonValue asset) {
		super.loadFields(asset);

		MultitileSizeComponent size = this.getComponent(MultitileSizeComponent.class);

		size.width = asset.getInt("tile_width", 2);
		size.height = asset.getInt("tile_height", 2);
	}

	@Override
	protected void place(short x, short y) {
		MultitileSizeComponent size = this.getComponent(MultitileSizeComponent.class);

		for (short xx = 0; xx < size.width; xx++) {
			for (short yy = 0; yy < size.height; yy++) {
				World.getInstance().setBlock(this.getComponent(IdComponent.class).id, (short) (xx + x), (short) (yy + y));
				World.getInstance().setData(TileHelper.multitile.create(xx, (short) (size.height - yy - 1)), (short) (xx + x), (short) (yy + y));
			}
		}
	}

	@Override
	protected boolean canBeUsed() {
		CameraComponent camera = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);
		Vector3 mouse = camera.camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));

		mouse.x /= SIZE;
		mouse.y /= SIZE;

		short x = (short) mouse.x;
		short y = (short) mouse.y;

		MultitileSizeComponent size = this.getComponent(MultitileSizeComponent.class);

		for (short xx = x; xx < x + size.width; xx++) {
			for (short yy = y; yy < y + size.height; yy++) {
				if (World.getInstance().getBlock(xx, yy) != null) {
					return false;
				}
			}
		}

		return true;
	}

	@Override
	public TileHelper getHelper() {
		return TileHelper.multitile;
	}


	@Override
	public int getNeighbors(short x, short y) {
		int data = World.getInstance().getData(x, y);
		return (short) ((MultitileHelper) this.getHelper()).getX(data);
	}

	@Override
	public int getVariant(int data) {
		return (short) ((MultitileHelper) this.getHelper()).getY(data);
	}
}