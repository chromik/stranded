package com.chromik.stranded.entity.entities.item.tile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.Item;
import com.chromik.stranded.entity.entities.item.ItemUseComponent;
import com.chromik.stranded.entity.entities.item.StackComponent;
import com.chromik.stranded.entity.entities.item.tile.helper.TileHelper;
import com.chromik.stranded.graphics.Graphics;
import com.chromik.stranded.util.binary.BinaryPacker;

import java.util.Objects;

public class Tile extends Item {

	public static short SIZE = 8;
	public static TextureRegion[][] cracks;

	public Tile(String id) {
		super(id);

		if (cracks == null) {
			// TODO: optimize?
			cracks = Assets.getTexture("util/tile_cracks").split(SIZE, SIZE);
		}

		this.addComponent(TileComponent.class);
		this.getComponent(ItemUseComponent.class).autoUse = true;
		this.getComponent(StackComponent.class).max = 999;
	}

	public TileHelper getHelper() {
		return TileHelper.main;
	}

	public void render(short x, short y, int data, float light) {
		this.render(x, y, data, light, this.getNeighbors(x, y));
	}

	public void render(short x, short y, int data, float light, int neighbors) {
		Graphics.batch.setColor(light, light, light, 1.0f);

		int variant = this.getVariant(data);
		int health = this.getHealth(data);

		Graphics.batch.draw(this.getComponent(TileComponent.class).tiles[variant][neighbors], x * SIZE, y * SIZE);

		if (health < 3 && health > 0) {
			// render the cracks
			Graphics.batch.draw(cracks[0][health - 1], x * SIZE, y * SIZE);
		}

		// reset the color
		Graphics.batch.setColor(1, 1, 1, 1);
	}

	@Override
	protected boolean onUse(Entity entity) {
		CameraComponent camera = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);
		Vector3 mouse = camera.camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));

		this.place((short) (mouse.x / SIZE), (short) (mouse.y / SIZE));

		return true;
	}

	protected void place(short x, short y) {

	}

	public int getNeighbors(short x, short y) {
		String id = this.getComponent(IdComponent.class).id;

		boolean top = this.shouldTile(this.getNeighbor(x, (short) (y + 1)), id);
		boolean right = this.shouldTile(this.getNeighbor((short) (x + 1), y), id);
		boolean bottom = this.shouldTile(this.getNeighbor(x, (short) (y - 1)), id);
		boolean left = this.shouldTile(this.getNeighbor((short) (x - 1), y), id);

		return BinaryPacker.pack(top, right, bottom, left);
	}

	protected String getNeighbor(short x, short y) {
		return null;
	}

	public int getVariant(int data) {
		return this.getHelper().getVariant(data);
	}

	public int getHealth(int data) {
		return this.getHelper().getBlockHealth(data);
	}

	protected boolean shouldTile(String neighbor, String self) {
		return Objects.equals(neighbor, self);
	}
}