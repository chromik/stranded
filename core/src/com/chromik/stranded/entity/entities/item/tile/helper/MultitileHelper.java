package com.chromik.stranded.entity.entities.item.tile.helper;

import com.chromik.stranded.util.binary.BitHelper;
import com.chromik.stranded.util.math.RandomGenerator;

public class MultitileHelper extends TileHelper {

	public int create(short x, short y) {
		int data = 0;

		data = this.setVariant(data, RandomGenerator.random(0, 2));
		data = this.setBlockHealth(data, 3);
		data = this.setWallHealth(data, 3);
		data = this.setX(data, x);
		data = this.setY(data, y);

		return data;
	}

	public int setX(int data, short x) {
		return BitHelper.putNumber(data, 7, 3, x);
	}

	public int setY(int data, short y) {
		return BitHelper.putNumber(data, 10, 3, y);
	}

	public int getX(int data) {
		return BitHelper.getNumber(data, 7, 3);
	}

	public int getY(int data) {
		return BitHelper.getNumber(data, 10, 3);
	}
}