package com.chromik.stranded.entity.entities.item;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.PositionComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.component.TextureComponent;
import com.chromik.stranded.entity.component.physics.AccelerationComponent;
import com.chromik.stranded.entity.component.physics.CollisionComponent;
import com.chromik.stranded.entity.component.physics.VelocityComponent;
import com.chromik.stranded.entity.entities.item.inventory.ItemComponent;
import com.chromik.stranded.graphics.Graphics;

public class ItemEntity extends Entity {
	public ItemEntity(Item item, int count) {
		super(ItemComponent.class, PositionComponent.class, SizeComponent.class, CollisionComponent.class,
			VelocityComponent.class, AccelerationComponent.class);

		ItemComponent itemComponent = this.getComponent(ItemComponent.class);

		itemComponent.item = item;
		itemComponent.count = (short) count;

		TextureComponent texture = this.getComponent(ItemComponent.class).item.getComponent(TextureComponent.class);
		SizeComponent size = this.getComponent(SizeComponent.class);

		size.width = texture.texture.getRegionWidth();
		size.height = texture.texture.getRegionHeight();
	}

	public ItemEntity(Item item) {
		this(item, 1);
	}

	@Override
	public void render() {
		TextureComponent texture = this.getComponent(ItemComponent.class).item.getComponent(TextureComponent.class);
		PositionComponent position = this.getComponent(PositionComponent.class);

		Graphics.batch.draw(texture.texture, position.x, position.y);
	}

	public ItemEntity setPosition(float x, float y) {
		PositionComponent position = this.getComponent(PositionComponent.class);

		position.x = x;
		position.y = y;

		return this;
	}
}