package com.chromik.stranded.entity.entities.item.tile;

import com.chromik.stranded.entity.asset.Assets;

public class TileableWall extends Wall {
	public TileableWall(String id) {
		super(id);
	}

	protected boolean shouldTile(String neighbor, String self) {
		Wall wall = (Wall) Assets.items.get(neighbor);
		return wall instanceof TileableWall;
	}
}