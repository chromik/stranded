package com.chromik.stranded.entity.entities.item.tile.interactable.fc;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.system.systems.UiSystem;
import com.chromik.stranded.entity.entities.item.tile.interactable.InteractableBlock;
import com.chromik.stranded.entity.entities.ui.window.UiWindow;

public class FC extends InteractableBlock {
	public FC(String id) {
		super(id);
	}

	public int getNeighbors(short x, short y) {
		// don't tile
		return 0;
	}

	public int getVariant(int data) {
		// only one varian
		return 0;
	}

	@Override
	public void onClick(int x, int y) {
		// TODO: keep track of the windows
		// If you click on another FC, open window for that FC, and do not close current
		// (they should have id, something like fc_x_y) (x and y in blocks!)
		UiWindow window = (UiWindow) UiSystem.getInstance().get("fc");

		if (window != null) {
			Engine.removeEntity(window);
		} else {
			window = new UiWindow(new Rectangle(96, 26, 128, 128));
			window.getComponent(IdComponent.class).id = "fc";

			Engine.addEntity(window);
		}
	}

	@Override
	public void renderUi() {
		super.renderUi(); // Render bg
		// TODO: render VRAM
	}
}