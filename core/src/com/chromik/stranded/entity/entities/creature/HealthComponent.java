package com.chromik.stranded.entity.entities.creature;

import com.chromik.stranded.core.io.FileReader;
import com.chromik.stranded.core.io.FileWriter;
import com.chromik.stranded.entity.component.Component;

import java.io.IOException;

public class HealthComponent extends Component {

	public short health = 0;
	public short healthMax = 0;

	@Override
	public void write(FileWriter writer) {
		try {
			writer.writeInt16(this.health);
			writer.writeInt16(this.healthMax);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	@Override
	public void load(FileReader reader) {
		try {
			this.health = reader.readInt16();
			this.healthMax = reader.readInt16();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}
}