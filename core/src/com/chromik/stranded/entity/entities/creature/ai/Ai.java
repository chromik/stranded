package com.chromik.stranded.entity.entities.creature.ai;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.entities.creature.Creature;
import com.chromik.stranded.entity.entities.creature.StateComponent;
import com.chromik.stranded.util.log.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class Ai {
	protected HashMap<String, Method> methods = new HashMap<>();

	public void update(Entity entity) {
		StateComponent state = entity.getComponent(StateComponent.class);
		Method method = this.methods.get(state.state);

		if (method != null) {
			try {
				method.invoke(this, entity, state.time);
			} catch (Exception exception) {
				if (exception.getClass().equals(InvocationTargetException.class)) {
					exception.getCause().printStackTrace();
				} else {
					exception.printStackTrace();
				}
				Log.error("Failed to call " + state.state + " state");
			}
		}

		state.time += 1;
	}

	public void collide(Entity self, Entity entity) {

	}

	protected void register(String state) {
		try {
			this.methods.put(state, this.getClass().getMethod(state, Creature.class, int.class));
		} catch (Exception exception) {
			exception.printStackTrace();
			Log.error("Failed to register " + state + " state");
		}
	}

	protected void registerAll(String... states) {
		for (String state : states) {
			this.register(state);
		}
	}
}