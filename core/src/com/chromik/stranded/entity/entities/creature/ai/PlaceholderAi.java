package com.chromik.stranded.entity.entities.creature.ai;

import com.chromik.stranded.entity.entities.creature.Creature;

public class PlaceholderAi extends Ai {
	public PlaceholderAi() {
		this.register("idle");
	}

	public void idle(Creature creature, int t) {

	}
}