package com.chromik.stranded.entity.entities.creature;

import com.chromik.stranded.core.Debug;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.component.*;
import com.chromik.stranded.entity.component.physics.AccelerationComponent;
import com.chromik.stranded.entity.component.physics.CollisionComponent;
import com.chromik.stranded.entity.component.physics.VelocityComponent;
import com.chromik.stranded.entity.entities.item.tile.Block;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.entity.entities.world.chunk.Chunk;
import com.chromik.stranded.graphics.Graphics;
import com.chromik.stranded.graphics.animation.Animation;

import java.util.Objects;

public class Creature extends Entity {
	public Creature(Class<? extends Component> ... types) {
		super(HealthComponent.class, PositionComponent.class, AccelerationComponent.class,
			VelocityComponent.class, TextureComponent.class, AnimationComponent.class, SizeComponent.class,
			CollisionComponent.class, IdComponent.class, StateComponent.class, AiComponent.class);

		this.addComponent(types);
		this.zIndex = 1;
	}

	@Override
	public void render() {
		AnimationComponent animation = this.getComponent(AnimationComponent.class);

		if (animation.current != null) {
			PositionComponent position = this.getComponent(PositionComponent.class);

			if (Debug.lightEnabled) {

				int x = Math.round(position.x / Block.SIZE);
				int y = Math.round(position.y / Block.SIZE);
				World world = World.getInstance();
				Chunk chunk = world.getChunkfor(x, y);
				boolean canSeeSky = y >= world.getHighest((short)x);

				float light;
				if (canSeeSky) {
					light = 1f;
				} else {

					int count = 0;
					float lightSum = 0;

					for (int i = x - 1; i <= x + 1; ++i) {
						for (int j = y - 2; j <= y; ++j) {
							if (chunk != null) {
								world.getChunk(i / Chunk.SIZE, j / Chunk.SIZE);
								lightSum += chunk.getLight(i % Chunk.SIZE, j % Chunk.SIZE);
								++ count;
							}
						}
					}
					light = lightSum / count;
				}

				Graphics.batch.setColor(light, light, light, 1);
			}
			animation.current.render(position.x, position.y);
		}

		// reset the color
		Graphics.batch.setColor(1, 1, 1, 1);
	}

	public void become(String state) {
		StateComponent stateComponent = this.getComponent(StateComponent.class);

		if (Objects.equals(state, stateComponent.state)) {
			return;
		}

		stateComponent.state = state;

		AnimationComponent animationComponent = this.getComponent(AnimationComponent.class);
		Animation animation = animationComponent.animations.get(state);

		if (animation == null) {
			animationComponent.current = animationComponent.animations.get("idle");
		} else {
			animationComponent.current = animation;
		}

		animationComponent.current.reset();
	}
}