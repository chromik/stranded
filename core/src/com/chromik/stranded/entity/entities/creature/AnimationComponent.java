package com.chromik.stranded.entity.entities.creature;

import com.chromik.stranded.entity.component.Component;
import com.chromik.stranded.graphics.animation.Animation;

import java.util.HashMap;

public class AnimationComponent extends Component {
	public HashMap<String, Animation> animations = new HashMap<>();
	public Animation current;
}