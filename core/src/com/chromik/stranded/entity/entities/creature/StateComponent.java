package com.chromik.stranded.entity.entities.creature;

import com.chromik.stranded.entity.component.Component;


public class StateComponent extends Component {
	public String state = "idle";
	public int time = 0;
}