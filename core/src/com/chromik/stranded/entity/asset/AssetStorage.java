package com.chromik.stranded.entity.asset;

import java.util.HashMap;

public class AssetStorage<T> {
	private HashMap<String, T> storage = new HashMap<>();


	public void add(String id, T value) {
		this.storage.put(id, value);
	}

	public T get(String id) {
		return this.storage.get(id);
	}

	@SuppressWarnings("unchecked")
	public <R> R getUnsafe(String id) {
		return (R) this.storage.get(id);
	}

	public HashMap<String, T> getAll() {
		return this.storage;
	}
}