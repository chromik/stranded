package com.chromik.stranded.entity.asset.factories;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.asset.AssetFactory;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.component.SizeComponent;
import com.chromik.stranded.entity.component.physics.CollisionComponent;
import com.chromik.stranded.entity.entities.creature.AiComponent;
import com.chromik.stranded.entity.entities.creature.AnimationComponent;
import com.chromik.stranded.entity.entities.creature.Creature;
import com.chromik.stranded.entity.entities.creature.HealthComponent;
import com.chromik.stranded.entity.entities.creature.ai.Ai;
import com.chromik.stranded.graphics.animation.Animation;
import com.chromik.stranded.graphics.animation.AnimationFrame;
import com.chromik.stranded.util.log.Log;

public class CreatureFactory extends AssetFactory<Creature> {

	@Override
	public Creature parse(JsonValue asset) {
		Creature creature = new Creature();

		if (asset.has("components")) {
			CreatureFactory.parseComponents(creature, asset);
		}

		if (asset.has("health")) {
			HealthComponent health = creature.getComponent(HealthComponent.class);

			health.health = asset.getShort("health");
			health.healthMax = health.health;
		}

		if (asset.has("animation")) {
			CreatureFactory.parseAnimations(creature, asset);
		}

		SizeComponent size = creature.getComponent(SizeComponent.class);

		size.width = asset.getShort("w", (short) 16);
		size.height = asset.getShort("h", (short) 16);

		IdComponent id = creature.getComponent(IdComponent.class);
		id.id = asset.name();

		AiComponent ai = creature.getComponent(AiComponent.class);
		String aiId = asset.getString("ai", "PlaceholderAi");

		try {
			Class type = Class.forName("com.chromik.stranded.entity.entities.creature.ai." + aiId);
			ai.ai = (Ai) type.newInstance();
		} catch (ClassNotFoundException exception) {
			Log.error("Class " + aiId + " is not found");
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		CollisionComponent collision = creature.getComponent(CollisionComponent.class);
		collision.weight = asset.getFloat("weight", 0.2f);

		return creature;
	}

	public Creature create(String id) {
		JsonValue data = Assets.creaturesData.get(id);

		if (data == null) {
			Log.error("Failed to find creature with id " + id);
			return null;
		}

		return this.parse(data);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void parseComponents(Creature creature, JsonValue asset) {
		for (JsonValue component : asset.get("components")) {
			if (component.isString()) {
				try {
					Class type = Class.forName("com.chromik.stranded." + component.toString());
					creature.addComponent(type);
				} catch (ClassNotFoundException exception) {
					Log.error("Class " + component.toString() + " is not found");
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			} else {
				Log.warning("Non-string component in " + asset.name());
			}
		}
	}

	private static void parseAnimations(Creature creature, JsonValue asset) {
		AnimationComponent animation = creature.getComponent(AnimationComponent.class);
		JsonValue root = asset.get("animation");
		TextureRegion region = Assets.getTexture("creatures/" + asset.name().replace(':', '_'));

		if (region == null) {
			Log.error("Did not find texture for " + asset.name());
		}

		for (JsonValue data : root) {
			Animation anim = new Animation();

			for (JsonValue frameData : data) {
				AnimationFrame frame = new AnimationFrame();

				frame.texture = new TextureRegion(region.getTexture(), region.getRegionX(), region.getRegionY(), region.getRegionWidth(), region.getRegionHeight());

				if (frame.texture != null) {
					frame.texture.setRegionX(frame.texture.getRegionX() + frameData.getInt("x", 0));
					frame.texture.setRegionY(frame.texture.getRegionY() + frameData.getInt("y", 0));
					frame.texture.setRegionWidth(frameData.getInt("w", 16));
					frame.texture.setRegionHeight(frameData.getInt("h", 16));
				}

				frame.delta = frameData.getFloat("delta", 1.0f);
				anim.addFrame(frame);
			}

			animation.animations.put(data.name(), anim);
		}

		animation.current = animation.animations.get("idle");
	}
}