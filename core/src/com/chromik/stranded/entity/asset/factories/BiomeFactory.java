package com.chromik.stranded.entity.asset.factories;

import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.asset.AssetFactory;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.IdComponent;
import com.chromik.stranded.entity.entities.world.biome.Biome;
import com.chromik.stranded.entity.entities.world.biome.BiomeGeneratorDataComponent;
import com.chromik.stranded.entity.entities.world.biome.BiomeIndexComponent;
import com.chromik.stranded.entity.entities.world.biome.BiomeSpawnComponent;

import java.util.Objects;

public class BiomeFactory extends AssetFactory<Biome> {

	@Override
	public Biome parse(JsonValue asset) {
		Biome biome = new Biome();

		IdComponent id = biome.getComponent(IdComponent.class);
		id.id = asset.name();

		if (asset.has("generation")) {
			generationProcess(asset, biome);
		}

		if (asset.has("spawns")) {
			spawnsProcess(asset, biome);
		}

		if (asset.has("index")) {
			indexProcess(asset, biome);
		}

		Assets.biomes.add(asset.name(), biome);

		return biome;
	}

	private void generationProcess(JsonValue asset, Biome biome) {
		BiomeGeneratorDataComponent generator = biome.getComponent(BiomeGeneratorDataComponent.class);
		JsonValue root = asset.get("generation");

		generator.soil = root.getString("soil", "st:dirt");
		generator.soil_wall = root.getString("soil_wall", "st:dirt_wall");
		generator.grass = root.getString("grass", "st:grass");
		generator.grass_wall = root.getString("grass_wall", "st:grass_wall");
	}

	private void spawnsProcess(JsonValue asset, Biome biome) {

		BiomeSpawnComponent spawns = biome.getComponent(BiomeSpawnComponent.class);
		JsonValue root = asset.get("spawns");

		spawns.maxSpawns = root.getInt("max_spawn", 1);

		if (root.has("spawn_rate")) {
			JsonValue rate = root.get("spawn_rate");

			if (rate.isNumber()) {
				spawns.spawnRate.put("any", rate.asFloat());
			} else {
				for (JsonValue event : rate) {
					spawns.spawnRate.put(
							event.name(), event.getFloat(0)
					);
				}
			}
		} else {
			spawns.spawnRate.put("any", 1.0f);
		}
	}

	private void indexProcess(JsonValue asset, Biome biome) {
		BiomeIndexComponent index = biome.getComponent(BiomeIndexComponent.class);
		JsonValue root = asset.get("index");

		for (JsonValue type : root) {
			if (Objects.equals(type.name, "any")) {
				String[] types = new String[type.size];

				for (int i = 0; i < type.size; i++) {
					types[i] = type.getString(i);
				}

				index.needed.put(types, (int) type.getFloat("count"));
			} else {
				index.needed.put(new String[] { type.name() }, (int) type.asFloat());
			}
		}
	}

}