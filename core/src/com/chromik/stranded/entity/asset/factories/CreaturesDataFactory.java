package com.chromik.stranded.entity.asset.factories;

import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.asset.AssetFactory;
import com.chromik.stranded.entity.asset.Assets;

public class CreaturesDataFactory extends AssetFactory<JsonValue> {

	@Override
	public JsonValue parse(JsonValue asset) {
		Assets.creaturesData.add(asset.name(), asset);
		return asset;
	}
}