package com.chromik.stranded.entity.asset.factories;

import com.badlogic.gdx.utils.JsonValue;
import com.chromik.stranded.entity.asset.AssetFactory;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.entities.item.Item;
import com.chromik.stranded.util.log.Log;

import java.lang.reflect.Constructor;

public class ItemFactory extends AssetFactory<Item> {

	@Override
	public Item parse(JsonValue asset) {
		String type = "com.chromik.stranded.entity.entities.item." + asset.getString("type", "Item");

		try {
			Class<?> clazz = Class.forName(type);
			Constructor<?> constructor = clazz.getConstructor(String.class);

			Item item = (Item) constructor.newInstance(asset.name);
			item.loadFields(asset);

			Assets.items.add(asset.name(), item);

			return item;
		} catch (Exception exception) {
			exception.printStackTrace();
			Log.error("Failed to create item " + asset.name);
		}

		return null;
	}
}