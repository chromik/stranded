package com.chromik.stranded.entity.asset;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.chromik.stranded.entity.asset.factories.*;
import com.chromik.stranded.entity.asset.storages.Biomes;
import com.chromik.stranded.entity.asset.storages.CreaturesData;
import com.chromik.stranded.entity.asset.storages.Items;
import com.chromik.stranded.entity.asset.storages.KeyBindings;
import com.chromik.stranded.util.log.Log;

public class Assets {
	public static Items items = new Items(); // item holder
	public static CreaturesData creaturesData = new CreaturesData(); // creatures data holder
	public static CreatureFactory creatures = new CreatureFactory(); // creates creatures
	public static KeyBindings keys = new KeyBindings(); // handles keys
	public static Biomes biomes = new Biomes(); // handles biomes
	public static BitmapFont f4; // small font
	public static BitmapFont f8; // big font

	private static AssetManager manager = new AssetManager(); // manager for textures

	public static void load() {
		Log.debug("Loading assets");

		loadFonts();
		loadTextures();
		loadJSON();
	}

	public static TextureRegion getTexture(String name) {
		return manager.get("atlas/textures.atlas", TextureAtlas.class).findRegion(name);
	}

	private static void loadFonts() {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("pico8.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameters = new FreeTypeFontGenerator.FreeTypeFontParameter();

		parameters.size = 4;
		f4 = generator.generateFont(parameters);

		parameters.size = 8;
		f8 = generator.generateFont(parameters);
	}

	private static void loadTextures() {
		manager.load("atlas/textures.atlas", TextureAtlas.class);
		manager.finishLoading();
	}

	private static void loadJSON() {
		AssetLoader.load("keys", new KeyFactory());
		AssetLoader.load("items", new ItemFactory());
		AssetLoader.load("creatures", new CreaturesDataFactory());
		AssetLoader.load("biomes", new BiomeFactory());
	}
}