package com.chromik.stranded;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.core.Version;
import com.chromik.stranded.core.boot.ArgumentParser;
import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.Players;
import com.chromik.stranded.entity.asset.Assets;
import com.chromik.stranded.entity.component.SaveableComponents;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.SystemMessage;
import com.chromik.stranded.entity.entities.item.inventory.InventoryComponent;
import com.chromik.stranded.entity.entities.ui.console.UiConsole;
import com.chromik.stranded.entity.entities.ui.inventory.UiInventory;
import com.chromik.stranded.entity.entities.world.ClockComponent;
import com.chromik.stranded.entity.entities.world.World;
import com.chromik.stranded.game.state.InGameState;
import com.chromik.stranded.game.state.State;
import com.chromik.stranded.graphics.GameScreenOptions;
import com.chromik.stranded.graphics.Graphics;
import com.chromik.stranded.util.input.Input;
import com.chromik.stranded.util.log.Log;

import java.util.HashSet;

public class Stranded extends ApplicationAdapter {

	private String title;
	private State state;

	@Override
	public void create() {

		SaveableComponents.init();
		try {
			ArgumentParser.parse();
		} catch (RuntimeException exception) {
			Log.error(exception.getMessage());
			Log.error("Failed to parse arguments!");
			Gdx.app.exit();
			return;
		}

// 		TODO: DO menu for game
//		MenuScreen menuScreen = new MenuScreen(this);
//		menuScreen.show();
//

		Gdx.input.setInputProcessor(Input.multiplexer);
		Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());

		this.title = "Stranded " + Version.STRING;
		this.setState(new InGameState());


		Assets.load();



		Engine.init();

		HashSet<Entity> players = Engine.getWithAllTypes(InventoryComponent.class);

		if (players.size() > 0) {
			Entity player = Players.clientPlayer;

			Engine.addEntity(new UiInventory(new Rectangle(5, GameScreenOptions.HEIGHT - 26, 150, 50), player.getComponent(InventoryComponent.class))); // Share the inventory
			Engine.addEntity(new UiConsole(new Rectangle(5, 30, 150, 16)));

			// TODO: TEMPORARLY
			InventoryComponent inv = player.getComponent(InventoryComponent.class);
			inv.inventory[1].item = Assets.items.get("st:fc_main");
			inv.inventory[1].count = 999;

			inv.inventory[2].item = Assets.items.get("st:stone");
			inv.inventory[2].count = 999;

			inv.inventory[3].item = Assets.items.get("st:chest");
			inv.inventory[3].count = 19;

			inv.inventory[4].item = Assets.items.get("st:dirt_wall");
			inv.inventory[4].count = 999;

			inv.inventory[5].item = Assets.items.get("st:grass_wall");
			inv.inventory[5].count = 100;

		} else {
			Log.warning("Failed to create UI inventory for the player");
		}
	}

	public void setState(State state) {
		this.state = state;
		//this.setScreen(state);

		Log.info("Set LT state to " + state.getClass());

		// TODO: save/load stuffs
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		Engine.sendMessage(SystemMessage.Type.WINDOW_RESIZED.create());
	}

	@Override
	public void render() {
		float delta = Gdx.graphics.getDeltaTime();
		this.state.update(delta);
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		Graphics.batch.begin();
		this.state.render();
		Graphics.batch.end();

		String title = this.title + " " + Gdx.graphics.getFramesPerSecond() + " FPS";

		if (World.getInstance() != null) {
			ClockComponent clock = World.getInstance().getComponent(ClockComponent.class);

			title += ", " + String.format("%02d", clock.hour) + ":" + String.format("%02d", (int) (clock.minute)) + ":" + String.format("%02d", (int) clock.second);
		}

		Gdx.graphics.setTitle(title);
	}

	@Override
	public void dispose() {
		super.dispose();
		Engine.sendMessage(SystemMessage.Type.SAVE.create());
	}
}