package com.chromik.stranded.util.geometry;

public class BlockVector {

    public int x, y;

    public BlockVector(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
