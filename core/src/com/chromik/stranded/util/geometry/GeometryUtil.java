package com.chromik.stranded.util.geometry;

import com.chromik.stranded.entity.entities.item.tile.Tile;

public class GeometryUtil {

    public static BlockVector convertToBlockVector(float x, float y) {
        return new BlockVector(Float.valueOf(x/ Tile.SIZE).intValue(), Float.valueOf(y/ Tile.SIZE).intValue());
    }

    public static double calculateDistance(BlockVector a, BlockVector b) {
        return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }
}
