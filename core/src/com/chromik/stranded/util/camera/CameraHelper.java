package com.chromik.stranded.util.camera;

import com.badlogic.gdx.math.Rectangle;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.entity.entities.item.tile.Tile;
import com.chromik.stranded.graphics.GameScreenOptions;

public class CameraHelper {

	public static Rectangle getCameraRectangle() {
		CameraComponent camera = CameraSystem.getInstance().get("main").getComponent(CameraComponent.class);

		int x = (int) (camera.camera.position.x - GameScreenOptions.WIDTH / 2);
		int y = (int) (camera.camera.position.y - GameScreenOptions.HEIGHT / 2);

		return new Rectangle(x, y, GameScreenOptions.WIDTH, GameScreenOptions.HEIGHT);
	}

	public static Rectangle getCameraRectangleInBlocks() {
		Rectangle rect = getCameraRectangle();

		rect.x = (float) Math.floor(rect.x / Tile.SIZE) - 1;
		rect.y = (float) Math.floor(rect.y / Tile.SIZE) - 1;
		rect.width = (float) Math.ceil(rect.width / Tile.SIZE) + 2;
		rect.height = (float) Math.ceil(rect.height / Tile.SIZE) + 2;

		return rect;
	}
}