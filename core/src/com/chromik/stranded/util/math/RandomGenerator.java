package com.chromik.stranded.util.math;

import com.chromik.stranded.entity.entities.world.generator.objects.Direction;

import java.util.concurrent.ThreadLocalRandom;

public class RandomGenerator {

	public static int random(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	public static boolean randomBoolean() {
		return random(0,1) >= 1;
	}


	public static Direction randomDirection() {
		return Direction.getDirection(random(0,4));
	}
}