package com.chromik.stranded.util.math;

public class NumberUtil {
	/**
	 * Returns middle value from all three
	 * @param x Number one
	 * @param y Number two
	 * @param z Number three
	 * @return The middle number
	 */
	public static float clamp(float x, float y, float z) {
		if (x > y) {
			float tmp = x;

			x = y;
			y = tmp;
		}

		return Math.max(x, Math.min(y, z));
	}

	public static int clamp(int x, int y, int z) {
		if (x > y) {
			int tmp = x;

			x = y;
			y = tmp;
		}

		return Math.max(x, Math.min(y, z));
	}
}