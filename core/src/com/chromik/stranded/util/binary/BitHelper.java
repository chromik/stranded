package com.chromik.stranded.util.binary;


public class BitHelper {

	public static int setBit(int data, int bit, int value) {
		if (value == 1) {
			data = data | (1 << bit);
		} else {
			data = data & ~(1 << bit);
		}

		return data;
	}

	public static int getBit(int data, int bit) {
		return (data >> bit) & 1;
	}

	public static int putNumber(int data, int bit, int size, int number) {
		for (int i = 0; i < size; i++) {
			data = setBit(data, bit + i, getBit(number, i));
		}

		return data;
	}

	public static int getNumber(int data, int bit, int size) {
		int num = 0;

		for (int i = 0; i < size; i++) {
			num += getBit(data, bit + i) << i;
		}

		return num;
	}
}