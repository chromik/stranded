package com.chromik.stranded.util.log;

public class LogLevel {
	private String name;
	private String color;
	private boolean enabled;

	public LogLevel(String name, String color, boolean enabled) {
		this.name = name;
		this.color = color;
		this.enabled = enabled;
	}


	public void print(String message) {
		if (this.enabled) {
			boolean windows = System.getProperty("os.name").startsWith("Windows");

			System.out.println(
				(windows ? "" : this.color) + this.name + " " +
					this.getStackTrace() + " " + message
			);
		}
	}

	private String getStackTrace() {
		return Thread.currentThread().getStackTrace()[4].getClassName() + ":" +
			Thread.currentThread().getStackTrace()[3].getLineNumber();
	}

	public void enable() {
		this.enabled = true;
	}

	public void disable() {
		this.enabled = true;
	}

	public String getName() {
		return this.name;
	}

	public String getColor() {
		return this.color;
	}

	public boolean isEnabled() {
		return this.enabled;
	}
}