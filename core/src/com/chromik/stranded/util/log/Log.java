package com.chromik.stranded.util.log;

public class Log {

	public static LogLevel[] levels;

	static {
		levels = new LogLevel[4];
		levels[0] = new LogLevel("INFO", AsciiColors.GREEN, true);
		levels[1] = new LogLevel("DEBUG", AsciiColors.BLUE, false);
		levels[2] = new LogLevel("ERROR", AsciiColors.RED, true);
		levels[3] = new LogLevel("WARNING", AsciiColors.PURPLE, true);
	}

	public static void info(String message) {
		levels[0].print(message);
	}

	public static void debug(String message) {
		levels[1].print(message);
	}

	public static void error(String message) {
		levels[2].print(message);
	}

	public static void warning(String message) {
		levels[3].print(message);
	}
}