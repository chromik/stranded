package com.chromik.stranded.util.struct;

public class Pair<T> {
	public final T left, right;

	public Pair(T left, T right) {
		this.left = left;
		this.right = right;
	}
	
	public boolean isSame() {
		return left.equals(right);
	}
}