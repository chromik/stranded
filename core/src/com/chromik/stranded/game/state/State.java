package com.chromik.stranded.game.state;

import com.badlogic.gdx.Screen;


public class State implements Screen {

	public void update(float delta) {
	}

	public void render() {
	}

	@Override
	public void show() {

	}

	@Override
	public void render(float delta) {

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}
}