package com.chromik.stranded.game.state;

import com.chromik.stranded.entity.Entity;
import com.chromik.stranded.entity.engine.Engine;
import com.chromik.stranded.entity.engine.system.systems.CameraSystem;
import com.chromik.stranded.entity.entities.camera.CameraComponent;
import com.chromik.stranded.graphics.Graphics;

import java.util.ArrayList;

public class InGameState extends State {

	@Override
	public void update(float delta) {
		Engine.update(delta);
	}

	@Override
	public void render() {
		ArrayList<Entity> entities = Engine.getEntities();
		Graphics.batch.setProjectionMatrix(CameraSystem.getInstance().get("main").getComponent(CameraComponent.class).camera.combined);

		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).render();
		}

		Graphics.batch.setProjectionMatrix(CameraSystem.getInstance().get("ui").getComponent(CameraComponent.class).camera.combined);

		for (int i = 0; i < entities.size(); i++) {
			entities.get(i).renderUi();
		}
	}
}