package com.chromik.stranded.core;

public class Version {
	private Version() {
	}

	public static double MAJOR = 0.0;
	public static short MINOR = 1;
	public static String NAME = "alpha";
	public static String STRING = "v" + Version.MAJOR + "." + Version.MINOR + " " + Version.NAME;
}