package com.chromik.stranded.core.boot.commands;

import com.chromik.stranded.core.Debug;
import com.chromik.stranded.core.boot.Command;
import com.chromik.stranded.util.log.Log;

public class DebugCommand extends Command {

	public DebugCommand() {
		super("debug", "d");
	}

	@Override
	public boolean call(String[] args) {
		Debug.enabled = true;
		Log.levels[1].enable(); // enable debug
		Log.debug("Debug mode is enabled");

		return true;
	}
}