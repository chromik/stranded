package com.chromik.stranded.core.boot.commands;

import com.chromik.stranded.core.Debug;
import com.chromik.stranded.core.boot.Command;

public class LightCommand extends Command{
	
	public LightCommand() {
		super("light", "l");
	}

	@Override
	public boolean call(String[] args) {
		Debug.lightEnabled = true;

		return true;
	}
}
