package com.chromik.stranded.core.boot.commands;

import com.chromik.stranded.core.Version;
import com.chromik.stranded.core.boot.Command;
import com.chromik.stranded.util.log.Log;

public class HelpCommand extends Command {
	public HelpCommand() {
		super("help", "h");
	}

	@Override
	public boolean call(String[] args) {
		Log.info("Stranded " + Version.STRING);
		return true;
	}
}