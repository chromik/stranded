package com.chromik.stranded.core.boot;

public class Command {

	private String name;
	private String shortName;

	public Command(String name, String shortName) {
		this.name = name;
		this.shortName = shortName;
	}

	public boolean call(String[] args) {
		return false;
	}

	public String getName() {
		return this.name;
	}

	public String getShortName() {
		return this.shortName;
	}
}