package com.chromik.stranded.core.boot;

import java.util.ArrayList;

import com.chromik.stranded.core.boot.commands.*;

public class ArgumentParser {

	private static String[] args; // command line arguments
	private static ArrayList<Command> commands = new ArrayList<>();

	static {
		commands.add(new HelpCommand());
		commands.add(new DebugCommand());
		commands.add(new PackTexturesCommand());
		commands.add(new LightCommand());
	}

	public static void setArgs(String[] newArgs) {
		ArgumentParser.args = newArgs;
	}

	public static void parse() throws RuntimeException {
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];

				boolean found = false;
				for (Command command : commands) {
					String name = command.getName();
					String shortName = command.getShortName();

					if (arg.equals("--" + name) || arg.equals("-" + shortName)) {
						command.call(args);
						found = true;
						break;
					}
				}

				if (!found) {
					throw new RuntimeException("Argument '" + arg + "' is unknown");
				}
			}
		}
	}
}