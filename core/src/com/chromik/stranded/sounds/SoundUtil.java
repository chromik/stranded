package com.chromik.stranded.sounds;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class SoundUtil {

    private static final Sound STONE_HIT = Gdx.audio.newSound(Gdx.files.internal("sounds/stone_hit.mp3"));
    private static final Sound PICKED = Gdx.audio.newSound(Gdx.files.internal("sounds/picked.mp3"));

    public static void playStoneHit() {
        STONE_HIT.play(0.2F);
    }

    public static void playPicked() {
        PICKED.play(0.35F);
    }
}
