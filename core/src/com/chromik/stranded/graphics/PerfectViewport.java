package com.chromik.stranded.graphics;

import com.badlogic.gdx.utils.viewport.Viewport;

public class PerfectViewport extends Viewport {
	@Override
	public void update(int screenWidth, int screenHeight, boolean centerCamera) {
		int size = Math.min(screenWidth / GameScreenOptions.WIDTH, screenHeight / GameScreenOptions.HEIGHT);

		int width = size * GameScreenOptions.WIDTH;
		int height = size * GameScreenOptions.HEIGHT;

		setScreenBounds((screenWidth - width) / 2,  (screenHeight - height) / 2, width, height);
		setWorldSize(GameScreenOptions.WIDTH, GameScreenOptions.HEIGHT);
		apply(centerCamera);
	}
}