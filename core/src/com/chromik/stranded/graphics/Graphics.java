package com.chromik.stranded.graphics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Graphics {
	public static SpriteBatch batch = new SpriteBatch();
	public static ShapeRenderer shapeRenderer = new ShapeRenderer();
}