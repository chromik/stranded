package com.chromik.stranded.graphics.animation;

import com.badlogic.gdx.graphics.g2d.TextureRegion;


public class AnimationFrame {
	public float delta;
	public TextureRegion texture;
}