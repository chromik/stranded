package com.chromik.stranded.graphics.animation;

import com.chromik.stranded.graphics.Graphics;

import java.util.ArrayList;

public class Animation {

	private ArrayList<AnimationFrame> frames = new ArrayList<>();
	private float time; // current time
	private int frame; // current animation frame

	public Animation() {
	}

	public void render(float x, float y) {
		AnimationFrame frame = this.frames.get(this.frame);
		if (frame != null) {
			Graphics.batch.draw(frame.texture, x, y);
		}
	}

	public void update(float delta) {
		this.time += delta;
		AnimationFrame frame = this.frames.get(this.frame);

		if (frame != null) {
			if (this.time > frame.delta) {
				this.time = 0.0f;
				this.frame = (this.frame + 1) % this.frames.size();
			}
		}
	}

	public void addFrame(AnimationFrame newFrame) {
		this.frames.add(newFrame);
	}

	public void reset() {
		this.frame = 0;
		this.time = 0;
	}
}