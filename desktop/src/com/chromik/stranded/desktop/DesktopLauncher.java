package com.chromik.stranded.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.chromik.stranded.Stranded;
import com.chromik.stranded.core.Version;
import com.chromik.stranded.core.boot.ArgumentParser;

public class DesktopLauncher {
	public static void main(String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "Stranded " + Version.STRING;
		config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
		config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
		config.fullscreen = true;
		config.vSyncEnabled = true;
		config.fullscreen = false;
		config.resizable = true;

		// config.addIcon("icon.png", Files.FileType.Internal);

		ArgumentParser.setArgs(args);

		new LwjglApplication(new Stranded());
	}
}