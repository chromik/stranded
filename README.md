## [DEV STAGE] Stranded
Stranded is game inspired by Terraria and programmed in Java using LibGDX library.

### Building

Clone the repo:

```bash
git clone https://gitlab.com/chromik/stranded
cd Stranded
```

Compile project:

```bash
./gradlew clean build dist
```

### Running
You should end up with a jar, placed in `desktop/build/libs/`. Run it.

There is a few options you can play with:

* Launching the game with `-nl` argument will disable lights and increase performance
* Launching the game with `-d` argument will enable developer/debug mode. Then you can press `/` to open dev console.

* To give yourself some items using console, you just need to type: `/give st:dirt 100`. `st:dirt` is dirt block id and `100` is the count. Full list of items can be found here: https://gitlab.com/chromik/stranded/blob/master/core/assets/data/items.json

#### TODO List

[TODO list can be found here](https://trello.com/b/qux6ebCU/tasks).

### License

```
MIT License

Copyright (c) 2018 Radek Chromík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
